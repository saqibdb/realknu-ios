//
//  AnswersQuestionsViewController.h
//  RealKnu
//
//  Created by Shirley-Mac3 on 2/8/17.
//  Copyright © 2017 Jason Macbook. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <BEMCheckBox/BEMCheckBox.h>


@interface AnswersQuestionsViewController : UIViewController
- (IBAction)backAction:(UIButton *)sender;

- (IBAction)submitAction:(UIButton *)sender;


// new
@property (weak, nonatomic) IBOutlet UITextView *txtAnsOne;
@property (weak, nonatomic) IBOutlet UITextView *txtAnsTwo;
@property (weak, nonatomic) IBOutlet UITextView *txtAnsThree;
@property (weak, nonatomic) IBOutlet UITextView *txtAnsFour;
@property (weak, nonatomic) IBOutlet UITextView *txtAnsFive;
@property (weak, nonatomic) IBOutlet UITextView *txtLenguageYouSpeak;

@property (weak, nonatomic) IBOutlet UIImageView *residentialCheckbox;
@property (weak, nonatomic) IBOutlet UIImageView *commercialCheckbox;
@property (weak, nonatomic) IBOutlet UIImageView *rentalCheckbox;

- (IBAction)residentialAgentAction:(UIButton *)sender;
- (IBAction)commercialAgentAction:(UIButton *)sender;
- (IBAction)rentalAgentAction:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet BEMCheckBox *englishCheckBox;
@property (weak, nonatomic) IBOutlet BEMCheckBox *italianCheckBox;
@property (weak, nonatomic) IBOutlet BEMCheckBox *spanishCheckBox;
@property (weak, nonatomic) IBOutlet BEMCheckBox *portugeseCheckBox;
@property (weak, nonatomic) IBOutlet BEMCheckBox *frenchCheckBox;
@property (weak, nonatomic) IBOutlet BEMCheckBox *russianCheckBox;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *emailHeightConstraint;
@property (weak, nonatomic) IBOutlet UITextField *emailText;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *submitViewTopSpace;


@end
