//
//  AnswersQuestionsViewController.m
//  RealKnu
//
//  Created by Shirley-Mac3 on 2/8/17.
//  Copyright © 2017 Jason Macbook. All rights reserved.
//

#import "AnswersQuestionsViewController.h"
#import "Backendless.h"
#import "AMSmoothAlertView.h"
#import "SVProgressHUD.h"
#import <INTULocationManager/INTULocationManager.h>
#import "SqbLogger.h"

@interface AnswersQuestionsViewController ()
@end

@implementation AnswersQuestionsViewController{
    BOOL isResidentialChecked;
    BOOL isCommercialChecked;
    BOOL isRentalChecked;
    NSString *languages ;
    BOOL isEmailGotten;
}
- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [[self view] endEditing:YES];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    
    isResidentialChecked = YES;
    isCommercialChecked = NO;
    
    
    if ([defaults objectForKey:@"QfistAndLastName"] != nil) {
        [self.txtAnsOne setText:[NSString stringWithFormat:@"%@",[defaults objectForKey:@"QfistAndLastName"]]];
    }
    else{
        self.txtAnsOne.text = @"";
    }
    
    if ([defaults objectForKey:@"companyName"] != nil) {
        [self.txtAnsTwo setText:[NSString stringWithFormat:@"%@",[defaults objectForKey:@"companyName"]]];
    }
    else{
        self.txtAnsTwo.text = @"";
    }

    if ([defaults objectForKey:@"licenceNumber"] != nil) {
        [self.txtAnsThree setText:[NSString stringWithFormat:@"%@",[defaults objectForKey:@"licenceNumber"]]];
    }
    else{
        self.txtAnsThree.text = @"";
    }

    if ([defaults objectForKey:@"bestPhoneNo"] != nil) {
        [self.txtAnsFour setText:[NSString stringWithFormat:@"%@",[defaults objectForKey:@"bestPhoneNo"]]];
    }
    else{
        self.txtAnsFour.text = @"";
    }

    if ([defaults objectForKey:@"speciality"] != nil) {
        [self.txtAnsFive setText:[NSString stringWithFormat:@"%@",[defaults objectForKey:@"speciality"]]];
    }
    else{
        self.txtAnsFive.text = @"";
    }
    
    if ([defaults objectForKey:@"Lenguage"] != nil) {
        languages = [defaults objectForKey:@"Lenguage"];

        [_englishCheckBox setOn:[languages containsString:@"English"]];
        [_italianCheckBox setOn:[languages containsString:@"Italian"]];
        [_spanishCheckBox setOn:[languages containsString:@"Spanish"]];
        [_portugeseCheckBox setOn:[languages containsString:@"Portugues"]];
        [_frenchCheckBox setOn:[languages containsString:@"French"]];
        [_russianCheckBox setOn:[languages containsString:@"Russian"]];

    }
    else{
        self.txtLenguageYouSpeak.text = @"";
    }
    
    
    isEmailGotten = [defaults boolForKey:@"isEmailGotten"];
    if (!isEmailGotten) {
        self.emailHeightConstraint.constant = 27.0;
        self.submitViewTopSpace.constant = 170.0;
    }
    else{
        self.emailHeightConstraint.constant = 0.0;
        self.submitViewTopSpace.constant = 147.5;
    }
    

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)GetCurrentLocation{
    [SVProgressHUD showWithStatus:@"Processing Request"];
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    [locMgr requestLocationWithDesiredAccuracy:INTULocationAccuracyCity
                                       timeout:10.0
                          delayUntilAuthorized:YES  // This parameter is optional, defaults to NO if omitted
                                         block:^(CLLocation *currentLocation, INTULocationAccuracy achievedAccuracy, INTULocationStatus status) {
                                             if (status == INTULocationStatusSuccess) {
                                                 [self registerNewUser:currentLocation];
                                                 
                                             }
                                             else if (status == INTULocationStatusTimedOut) {
                                                 [SVProgressHUD dismiss];
                                                 AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"" andText:@"TimeOut" andCancelButton:NO forAlertType:AlertFailure];
                                                 
                                                 [alert show];
                                             }
                                             else {
                                                 [SVProgressHUD dismiss];
                                                 AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error!" andText:@"Error in Finding your location." andCancelButton:NO forAlertType:AlertFailure];
                                                 
                                                 [alert show];
                                             }
                                         }];
    
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
-(void)registerNewUser:(CLLocation *)location

{
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    
    BackendlessUser *user = [BackendlessUser new];
    [user setProperty:@"name" object:[defaults objectForKey:@"username"]];
    NSString *email = [defaults objectForKey:@"email"];
    
    if (!isEmailGotten) {
        email = self.emailText.text;
    }
    
    
    
    [user setProperty:@"email" object:email];
    [user setProperty:@"password" object:[defaults objectForKey:@"password"]];
    [user setProperty:@"type" object:@"agent"];

    [user setProperty:@"longitude" object:[NSString stringWithFormat:@"%f",location.coordinate.longitude]];
    [user setProperty:@"latitude" object:[NSString stringWithFormat:@"%f",location.coordinate.latitude]];
    // answer questions
    [user setProperty:@"QfistAndLastName" object:self.txtAnsOne.text];
    [user setProperty:@"companyName" object:self.txtAnsTwo.text];
    [user setProperty:@"licenceNumber" object:self.txtAnsThree.text];
    [user setProperty:@"bestPhoneNo" object:self.txtAnsFour.text];
    [user setProperty:@"loginStatus" object:@"loggedOff"];

    NSString *agentType = @"";
    NSString *speciality = @"";

    if (isResidentialChecked == YES) {
        if (agentType.length) {
            agentType = [NSString stringWithFormat:@"%@,RR",agentType];
            speciality = [NSString stringWithFormat:@"%@,Residential",speciality];
        }
        else{
            agentType = @"RR";
            speciality = @"Residential";
        }
    }
    
    if (isCommercialChecked == YES) {
        if (agentType.length) {
            agentType = [NSString stringWithFormat:@"%@,RC",agentType];
            speciality = [NSString stringWithFormat:@"%@,Commercial",speciality];

        }
        else{
            agentType = @"RC";
            speciality = @"Commercial";

        }
    }
    
    if (isRentalChecked == YES) {
        if (agentType.length) {
            agentType = [NSString stringWithFormat:@"%@,Ar",agentType];
            speciality = [NSString stringWithFormat:@"%@,Rental",speciality];
        }
        else{
            agentType = @"Ar";
            speciality = @"Rental";
        }
    }
    
    languages = @"";
    if (_englishCheckBox.on) {
        if (languages.length) {
            languages = [NSString stringWithFormat:@"%@,English",languages];
        }
        else{
            languages = @"English";
        }
    }
    
    if (_italianCheckBox.on) {
        if (languages.length) {
            languages = [NSString stringWithFormat:@"%@,Italian",languages];
        }
        else{
            languages = @"Italian";
        }
    }
    if (_spanishCheckBox.on) {
        if (languages.length) {
            languages = [NSString stringWithFormat:@"%@,Spanish",languages];
        }
        else{
            languages = @"Spanish";
        }
    }
    if (_portugeseCheckBox.on) {
        if (languages.length) {
            languages = [NSString stringWithFormat:@"%@,Portugues",languages];
        }
        else{
            languages = @"Portugues";
        }
    }
    if (_frenchCheckBox.on) {
        if (languages.length) {
            languages = [NSString stringWithFormat:@"%@,French",languages];
        }
        else{
            languages = @"French";
        }
    }
    if (_russianCheckBox.on) {
        if (languages.length) {
            languages = [NSString stringWithFormat:@"%@,Russian",languages];
        }
        else{
            languages = @"Russian";
        }
    }
    
    
    [user setProperty:@"Lenguage" object:languages];

    [user setProperty:@"agentType" object:agentType];
    [user setProperty:@"speciality" object:speciality];

    
    
    [backendless.userService registering:user response:^(BackendlessUser *userRegistered) {
        [SVProgressHUD dismiss];
        //[[NSUserDefaults standardUserDefaults] removePersistentDomainForName:[[NSBundle mainBundle] bundleIdentifier]];
        backendless.userService.currentUser = userRegistered;
        [backendless.userService setStayLoggedIn:YES] ;

        [SqbLogger setLastAgentUserName:[user getProperty:@"name"]] ;
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];

        NSLog(@"Password is %@",[defaults objectForKey:@"password"]);
        
        [SqbLogger setAgentPassword:[defaults objectForKey:@"password"]] ;

        [self performSegueWithIdentifier:@"ansQToThankU" sender:self];
    }
                                   error:^(Fault *error) {
                                       
                                       if ([error.faultCode isEqualToString:@"3033"]){
                                           [SVProgressHUD dismiss];
                                           AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Login Failed!!!" andText:@"This UserName is already Registered as an Agent, Please Register with another User Name on previous screen." andCancelButton:NO forAlertType:AlertFailure];
                                           [alert show];
                                           return;
                                       }
                                       
                                       if ([error.faultCode isEqualToString:@"-1009"]){
                                           [SVProgressHUD dismiss];
                                           AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Login Failed!!!" andText:@"The Internet connection appears to be offline." andCancelButton:NO forAlertType:AlertFailure];
                                           [alert show];
                                           return;
                                       }
                                       else{
                                           NSLog(@"Error At Register %@", error.description);
                                           NSString *errorMsg = [NSString stringWithFormat:@"Error At Register. Details = %@", error.description];
                                           
                                           
                                           NSLog(@"FAULT = %@ <%@>", error.message, error.detail);
                                           
                                           [SqbLogger logIt:[NSString stringWithFormat:@"%@ - %@" , error.faultCode , error.message ]] ;
                                           
                                           AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Oops!!!" andText:@"Something Unexpected has happened. Please Send Report to Developers." andCancelButton:YES forAlertType:AlertInfo andColor:[UIColor purpleColor] withCompletionHandler:^(AMSmoothAlertView *alertView, UIButton *btton) {
                                               if(btton == alertView.defaultButton) {
                                                   NSLog(@"Default");
                                                   [SqbLogger submitCrashReport:[NSString stringWithFormat:@"%@ - %@" , error.faultCode , error.message ]];
                                                   
                                                   [alert dismissAlertView];
                                                   
                                               }
                                               else {
                                                   NSLog(@"Others");
                                                   [alert dismissAlertView];
                                                   
                                               }
                                           }];
                                           [alert.defaultButton setTitle:@"Send Report" forState:UIControlStateNormal];
                                           [alert.defaultButton.titleLabel setFont: [alert.defaultButton.titleLabel.font fontWithSize: 12]];
                                           
                                           [alert.cancelButton setTitle:@"Close" forState:UIControlStateNormal];
                                           [alert.cancelButton.titleLabel setFont: [alert.defaultButton.titleLabel.font fontWithSize: 12]];
                                           
                                           
                                           [alert show];
                                           
                                           [SVProgressHUD dismiss];
                                       }
                                       
                                       
                                      
                                       
                                   }];
    
}

- (IBAction)backAction:(UIButton *)sender {
    languages = @"";
    if (_englishCheckBox.on) {
        if (languages.length) {
            languages = [NSString stringWithFormat:@"%@,English",languages];
        }
        else{
            languages = @"English";
        }
    }
    
    if (_italianCheckBox.on) {
        if (languages.length) {
            languages = [NSString stringWithFormat:@"%@,Italian",languages];
        }
        else{
            languages = @"Italian";
        }
    }
    if (_spanishCheckBox.on) {
        if (languages.length) {
            languages = [NSString stringWithFormat:@"%@,Spanish",languages];
        }
        else{
            languages = @"Spanish";
        }
    }
    if (_portugeseCheckBox.on) {
        if (languages.length) {
            languages = [NSString stringWithFormat:@"%@,Portugues",languages];
        }
        else{
            languages = @"Portugues";
        }
    }
    if (_frenchCheckBox.on) {
        if (languages.length) {
            languages = [NSString stringWithFormat:@"%@,French",languages];
        }
        else{
            languages = @"French";
        }
    }
    if (_russianCheckBox.on) {
        if (languages.length) {
            languages = [NSString stringWithFormat:@"%@,Russian",languages];
        }
        else{
            languages = @"Russian";
        }
    }
    
    
    
    
    
    
    
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    [defaults setObject:self.txtAnsOne.text forKey:@"QfistAndLastName"];
    [defaults setObject:self.txtAnsTwo.text forKey:@"companyName"];
    [defaults setObject:self.txtAnsThree.text forKey:@"licenceNumber"];
    [defaults setObject:self.txtAnsFour.text forKey:@"bestPhoneNo"];
    [defaults setObject:self.txtAnsFive.text forKey:@"speciality"];
    [defaults setObject:languages forKey:@"Lenguage"];
    [defaults synchronize];
    
    [self.navigationController popViewControllerAnimated:YES];
    
}
- (IBAction)submitAction:(UIButton *)sender {
    
    

    
    
    NSString *agentType = @"";
    if (isResidentialChecked == YES) {
        if (agentType.length) {
            agentType = [NSString stringWithFormat:@"%@,RR",agentType];
        }
        else{
            agentType = @"RR";
        }
    }
    
    if (isCommercialChecked == YES) {
        if (agentType.length) {
            agentType = [NSString stringWithFormat:@"%@,RC",agentType];
        }
        else{
            agentType = @"RC";
        }
    }
    
    if (isRentalChecked == YES) {
        if (agentType.length) {
            agentType = [NSString stringWithFormat:@"%@,Ar",agentType];
        }
        else{
            agentType = @"Ar";
        }
    }
    self.txtAnsFive.text = agentType;
    
    
    if (isCommercialChecked == NO && isResidentialChecked == NO && isRentalChecked == NO) {
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"" andText:@"Please Select atleast 1 Agent type" andCancelButton:NO forAlertType:AlertInfo];
        [alert show];
        return;
    }
    
    if ((isEmailGotten == NO) && (self.emailText.text.length == 0)) {
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"No Email" andText:@"Please Enter an Email" andCancelButton:NO forAlertType:AlertInfo];
        [alert show];
        return;
    }
    
    if ((isEmailGotten == NO) && ([self NSStringIsValidEmail:self.emailText.text] == NO)) {
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Email Not Valid" andText:@"Please Enter a Valid Email" andCancelButton:NO forAlertType:AlertInfo];
        [alert show];
        return;
    }
    
    
    if (!self.txtAnsOne.text.length || !self.txtAnsTwo.text.length || !self.txtAnsThree.text.length || !self.txtAnsFour.text.length) {
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"" andText:@"Please Answer all the Questions" andCancelButton:NO forAlertType:AlertInfo];
        
        [alert show];
        
    }
    else{
        [self GetCurrentLocation];
        
    }
}
- (IBAction)residentialAgentAction:(UIButton *)sender {
    if (isResidentialChecked) {
        isResidentialChecked = NO;
        self.residentialCheckbox.image = [UIImage imageNamed:@"uncheckIcon"];
    }
    else{
        isResidentialChecked = YES;
        self.residentialCheckbox.image = [UIImage imageNamed:@"checkIcon"];
    }
}

- (IBAction)commercialAgentAction:(UIButton *)sender {
    if (isCommercialChecked) {
        isCommercialChecked = NO;
        self.commercialCheckbox.image = [UIImage imageNamed:@"uncheckIcon"];
    }
    else{
        isCommercialChecked = YES;
        self.commercialCheckbox.image = [UIImage imageNamed:@"checkIcon"];
    }
}

- (IBAction)rentalAgentAction:(UIButton *)sender {
    if (isRentalChecked) {
        isRentalChecked = NO;
        self.rentalCheckbox.image = [UIImage imageNamed:@"uncheckIcon"];
    }
    else{
        isRentalChecked = YES;
        self.rentalCheckbox.image = [UIImage imageNamed:@"checkIcon"];
    }
}


-(BOOL) NSStringIsValidEmail:(NSString *)checkString{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}
@end
