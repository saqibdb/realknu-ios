//
//  AppDelegate.h
//  RealKnu
//
//  Created by Shirley-Mac3 on 2/1/17.
//  Copyright © 2017 Jason Macbook. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

