//
//  AppDelegate.m
//  RealKnu
//
//  Created by Shirley-Mac3 on 2/1/17.
//  Copyright © 2017 Jason Macbook. All rights reserved.
//

#import "AppDelegate.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import "Backendless.h"
#import <INTULocationManager/INTULocationManager.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>


@import GoogleMaps;
@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    //[[NSUserDefaults standardUserDefaults] removePersistentDomainForName:[[NSBundle mainBundle] bundleIdentifier]];

    [backendless initApp:@"306134D4-1ACC-0A6C-FF03-18DFC2BB9900" secret:@"FB0C16C0-60C7-654F-FF6B-301205F57800" version:@"v1"];
    [Fabric with:@[[Crashlytics class]]];
    //[IQKeyboardManager sharedManager].enable = YES;
    [GMSServices provideAPIKey:@"AIzaSyDtEM4wUijtLovnD_opwj2-OlBPf_I9Pvs"];
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    /*
    
    UIBackgroundTaskIdentifier counterTask;

    counterTask = [[UIApplication sharedApplication]
                   beginBackgroundTaskWithExpirationHandler:^{
                       
                       BackendlessUser *user = backendless.userService.currentUser;
                       NSString *userType = [user getProperty:@"type"];
                       
                       if ([userType isEqualToString:@"agent"]) {
                           // Get current location and update in backendless
                           [self getCurrentLocation];
                       }
                   }];
     */
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    
    
    NSLog(@"Application is about to Terminate");
    
    if (backendless.userService.currentUser) {
        
        
        @try {
            [backendless.userService.currentUser updateProperties:@{@"loginStatus" : @"loggedOff"}];

            BackendlessUser *updatedUser = [backendless.userService update:backendless.userService.currentUser];

            NSLog(@"User has been updated (SYNC): %@", updatedUser);
        }
        @catch (Fault *fault) {
            NSLog(@"FAULT: %@", fault);
        }
        
        
        
        
    }
    
    
    
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - methods
-(void)getCurrentLocation{
    
    
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    
    [locMgr subscribeToLocationUpdatesWithDesiredAccuracy:INTULocationAccuracyBlock block:^(CLLocation *currentLocation, INTULocationAccuracy achievedAccuracy, INTULocationStatus status) {
        
        if (status == INTULocationStatusSuccess) {
            [self updateAgentlocation:currentLocation];
        }
        else {
            
        }
    }];
    
    [locMgr requestLocationWithDesiredAccuracy:INTULocationAccuracyCity
                                       timeout:10.0
                          delayUntilAuthorized:YES  // This parameter is optional, defaults to NO if omitted
                                         block:^(CLLocation *currentLocation, INTULocationAccuracy achievedAccuracy, INTULocationStatus status) {
                                             if (status == INTULocationStatusSuccess) {
                                                 [self updateAgentlocation:currentLocation];
                                             }
                                             else if (status == INTULocationStatusTimedOut) {                                                 
                                                 
                                             }
                                             else {
                                                 
                                            }
                                         }];
}

-(void)updateAgentlocation:(CLLocation *)loc {
    
    if (backendless.userService.currentUser) {
        [backendless.userService.currentUser setProperty:@"longitude" object:[NSString stringWithFormat:@"%f",loc.coordinate.longitude]];
        [backendless.userService.currentUser setProperty:@"latitude" object:[NSString stringWithFormat:@"%f",loc.coordinate.latitude]];
        int i=0;
        
        [backendless.userService.currentUser setProperty:@"licenceNumber" object:[NSString stringWithFormat:@"%d",i+1]];

        [backendless.persistenceService save:backendless.userService.currentUser];
    }
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    
    BOOL handled = [[FBSDKApplicationDelegate sharedInstance] application:application
                                                                  openURL:url
                                                        sourceApplication:sourceApplication
                                                               annotation:annotation
                    ];
    // Add any custom logic here.
    return handled;
}
@end
