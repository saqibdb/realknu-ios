//
//  ChangeCityViewController.h
//  RealKnu
//
//  Created by Shirley-Macbook on 17/10/2017.
//  Copyright © 2017 Jason Macbook. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChangeCityViewController : UIViewController <UITableViewDelegate , UITableViewDataSource, UISearchBarDelegate>

@property (weak, nonatomic) IBOutlet UITableView *cityTableView;

@property (weak, nonatomic) IBOutlet UISearchBar *citySearchBar;



- (IBAction)backAction:(id)sender;

@end
