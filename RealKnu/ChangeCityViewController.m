//
//  ChangeCityViewController.m
//  RealKnu
//
//  Created by Shirley-Macbook on 17/10/2017.
//  Copyright © 2017 Jason Macbook. All rights reserved.
//

#import "ChangeCityViewController.h"
#import "MapViewController.h"

@interface ChangeCityViewController (){
    NSMutableArray *allUSCities;
    NSMutableArray *allUSCitiesOriginal;

}

@end

@implementation ChangeCityViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.cityTableView.delegate = self;
    self.cityTableView.dataSource = self;
    self.citySearchBar.delegate = self;
    [self getAllCities];
}


-(void)getAllCities {
    NSError *error = nil;
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"citiesUS"
                                                         ofType:@"json"];
    NSData *dataFromFile = [NSData dataWithContentsOfFile:filePath];
    NSArray *citiesArray = [NSJSONSerialization JSONObjectWithData:dataFromFile
                                                         options:kNilOptions
                                                           error:&error];
    if (error != nil) {
        NSLog(@"Error: was not able to load messages.");
        return;
    }
    
    
    NSLog(@"Cities Count = %lu" , (unsigned long)citiesArray.count);
    
     allUSCities = [[NSMutableArray alloc] init];
    
    for (NSDictionary *cityDict in citiesArray) {
        
        NSString *country = [cityDict objectForKey:@"country"];
        if ([country isEqualToString:@"US"]) {
            /*
            
            
            NSPredicate *aPredicate = [NSPredicate predicateWithFormat:@"%K LIKE[c] %@", @"name", [cityDict objectForKey:@"name"]];
            
            NSArray *theFilteredArray = [allUSCities filteredArrayUsingPredicate:aPredicate];
            
            if ([theFilteredArray count]){
                NSLog(@"Dictionay Exists");
            }else{
                NSLog(@"Dictionay does not Exists");
                [allUSCities addObject:cityDict];
            }
            */
            [allUSCities addObject:cityDict];

            
        }
    }
    allUSCitiesOriginal = [[NSMutableArray alloc] initWithArray:allUSCities];
    NSLog(@"US Cities Count = %lu" , (unsigned long)allUSCities.count);
    
    
   // NSString *jsonString = [self jsonWithArray:allUSCities];
    
   // NSLog(@"Unique Us Cities Are %@" , jsonString);
    
    
    [self.cityTableView reloadData];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return allUSCities.count;
}

- (NSString*)jsonWithArray :(NSArray *)array
{
    NSString* json = nil;
    
    NSError* error = nil;
    NSData *data = [NSJSONSerialization dataWithJSONObject:array options:NSJSONWritingPrettyPrinted error:&error];
    json = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
    return (error ? nil : json);
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    NSDictionary *cityDict = [allUSCities objectAtIndex:indexPath.row];
    NSString *cityName = [cityDict objectForKey:@"name"];
    cell.textLabel.text = cityName;
    return cell;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    NSLog(@"Search Text = %@" , searchText);
    
    
    allUSCities = [[NSMutableArray alloc] init];
    
    
    if (searchText.length) {
        for (NSDictionary *cityDict in allUSCitiesOriginal) {
            
            NSString *cityName = [cityDict objectForKey:@"name"];
            if ([[cityName lowercaseString] containsString:[searchText lowercaseString]]) {
                [allUSCities addObject:cityDict];
            }
        }
    }
    else{
        allUSCities = [[NSMutableArray alloc] initWithArray:allUSCitiesOriginal];

    }
    
    
    
    [self.cityTableView reloadData];
    
    
    
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSArray *allControllers = self.navigationController.viewControllers;
    
    for (UIViewController *controller in allControllers) {
        if ([controller isKindOfClass:[MapViewController class]]) {
            
            MapViewController *mapController = (MapViewController *)controller;
            mapController.selectedCity = [allUSCities objectAtIndex:indexPath.row];
            [self.navigationController popViewControllerAnimated:YES];
            break;
        }
    }
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}
@end
