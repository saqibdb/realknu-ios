//
//  ConnectPopUp.h
//  RealKnu
//
//  Created by Shirley-Mac3 on 2/7/17.
//  Copyright © 2017 Jason Macbook. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ConnectPopUp : UIViewController
    @property (weak, nonatomic) IBOutlet UILabel *lblTitleText;
    @property (weak, nonatomic) IBOutlet UIButton *yesBtn;
    @property (weak, nonatomic) IBOutlet UIButton *noBtn;
@property (weak, nonatomic) IBOutlet UIButton *declineBtn;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblEmail;
@property (weak, nonatomic) IBOutlet UILabel *lblPhone;
@property (weak, nonatomic) IBOutlet UIImageView *image;
@property (weak, nonatomic) IBOutlet UILabel *companyName;
@property (weak, nonatomic) IBOutlet UILabel *licenceNo;
@property (weak, nonatomic) IBOutlet UILabel *speciality;
@property (weak, nonatomic) IBOutlet UILabel *lenguage;

@property (weak, nonatomic) IBOutlet UIButton *btnPhone;

@property (weak, nonatomic) IBOutlet UIButton *callBtn;
@property (weak, nonatomic) IBOutlet UIButton *smsBtn;
@property (weak, nonatomic) IBOutlet UIButton *whatsAppBtn;




- (IBAction)phoneAction:(id)sender;


@end
