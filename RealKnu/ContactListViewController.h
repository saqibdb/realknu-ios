//
//  ContactListViewController.h
//  RealKnu
//
//  Created by Shirley-Mac3 on 2/2/17.
//  Copyright © 2017 Jason Macbook. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Backendless.h"
#import "SWTableViewCell.h"
#import <iCarousel/iCarousel.h>
@interface ContactListViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,SWTableViewCellDelegate , iCarouselDelegate, iCarouselDataSource, CLLocationManagerDelegate>
- (IBAction)backAction:(UIButton *)sender;
- (IBAction)menuAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIView *popUpView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong) BackendlessUser *selectedUser;
@property (weak, nonatomic) IBOutlet UIView *triangleSpriteView;

@property (weak, nonatomic) IBOutlet iCarousel *popUpCarousel;
@property (nonatomic,retain) CLLocationManager *locationManager;
@property (weak, nonatomic) IBOutlet UILabel *loggedInAsText;


@end
