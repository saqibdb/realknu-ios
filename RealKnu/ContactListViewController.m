//
//  ContactListViewController.m
//  RealKnu
//
//  Created by Shirley-Mac3 on 2/2/17.
//  Copyright © 2017 Jason Macbook. All rights reserved.
//
#import "CustomerProfileViewController.h"
#import "ContactListViewController.h"
#import "UserCellTableViewCell.h"
#import "Backendless.h"
#import "AMSmoothAlertView.h"
#import "SVProgressHUD.h"
#import "Inbox.h"
#import "UIImageView+WebCache.h"
#import <INTULocationManager/INTULocationManager.h>
#import "ConnectPopUp.h"
#import "TSMessageView.h"

@interface ContactListViewController (){
    
    NSMutableArray *allAgentsArray ;
    bool isFirstTime;
    
    NSMutableArray *allNewRequests ;
    INTULocationManager *locMgr;
    
    INTULocationRequestID requestID ;
}

@end

@implementation ContactListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view layoutIfNeeded];
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    [self.popUpView setHidden:YES];
    [self.triangleSpriteView setHidden:YES];
    isFirstTime = YES;
    [self maskTriananlge:self.triangleSpriteView];
    allAgentsArray = [[NSMutableArray alloc]init];
    allNewRequests = [[NSMutableArray alloc]init];
    [self fetchUsers];
    
  
    //[NSTimer scheduledTimerWithTimeInterval:30.0 target:self selector:@selector(reloadTable) userInfo:nil repeats:YES];
     //[NSTimer scheduledTimerWithTimeInterval:60.0 target:self selector:@selector(GetCurrentLocation) userInfo:nil repeats:YES];
    
    self.popUpCarousel.delegate = self ;
    self.popUpCarousel.dataSource = self ;
    self.popUpCarousel.type = iCarouselTypeTimeMachine ;

    
    self.loggedInAsText.text = [NSString stringWithFormat:@"LOGGED IN AS AGENT {%@}",[backendless.userService.currentUser getProperty:@"QfistAndLastName"] ];
    //    NotificationCenter.default.addObserver(self, selector: #selector(reinstateBackgroundTask), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)

    // Do any additional setup after loading the view.
}


-(void)viewDidAppear:(BOOL)animated{
    [self.popUpView setHidden:YES];
    [self.triangleSpriteView setHidden:YES];
    [self GetCurrentLocation];
}

-(void)viewWillDisappear:(BOOL)animated{
    [locMgr cancelLocationRequest:requestID] ;
}

- (NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel{
    return allNewRequests.count ;
}
- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(nullable UIView *)view{
    UIView *rootController = [[[NSBundle mainBundle] loadNibNamed:@"CarouselPopUp" owner:self options:nil] objectAtIndex:0];
    
    Inbox *request = allNewRequests[index];

    
    
    UIImageView *userImageView = [rootController viewWithTag:1] ;
    UILabel *nameText = [rootController viewWithTag:2] ;
    UILabel *lblEmail = [rootController viewWithTag:3] ;
    UILabel *lblPhone = [rootController viewWithTag:4] ;
    UILabel *companyName = [rootController viewWithTag:5] ;
    UILabel *licenceNo = [rootController viewWithTag:6] ;
    UILabel *speciality = [rootController viewWithTag:7] ;
    UILabel *lenguage = [rootController viewWithTag:8] ;

    UIButton *emailBtn = [rootController viewWithTag:10];
    UIButton *phoneBtn = [rootController viewWithTag:11];
    UIButton *closeBtn = [rootController viewWithTag:12];

    
    UIButton *whatsappBtn = [rootController viewWithTag:110];

    [userImageView.layer setCornerRadius:userImageView.frame.size.height/2];
    userImageView.clipsToBounds=YES;
    nameText.text = request.sender.name ;
    lblEmail.text =[NSString stringWithFormat:@"Email:%@",request.sender.email] ;
    lblPhone.text = [NSString stringWithFormat:@"Phone:%@",[request.sender getProperty:@"phone"]];
    
    [emailBtn addTarget:self action:@selector(emailAction:) forControlEvents:UIControlEventTouchUpInside ];
    [phoneBtn addTarget:self action:@selector(phoneAction:) forControlEvents:UIControlEventTouchUpInside ];
    [closeBtn addTarget:self action:@selector(closeAction:) forControlEvents:UIControlEventTouchUpInside ];
    
    
    [whatsappBtn addTarget:self action:@selector(whatsappAction:) forControlEvents:UIControlEventTouchUpInside ];

    
    
    emailBtn.tag = index;
    phoneBtn.tag = index;
    closeBtn.tag = index;
    whatsappBtn.tag = index;

    //
    //companyName.text = [NSString stringWithFormat:@"Company: %@",[request.sender getProperty:@"companyName"]];
    //licenceNo.text = [NSString stringWithFormat:@"Licence No: %@",[request.sender getProperty:@"licenceNumber"]];
    //speciality.text = [NSString stringWithFormat:@"Speciality:%@",[request.sender getProperty:@"speciality"]];
    //lenguage.text = [NSString stringWithFormat:@"Lenguage: %@",[request.sender getProperty:@"Lenguage"]];
    
    if (![[request.sender getProperty:@"profileImage"] isEqual:[NSNull null]]) {
        [userImageView sd_setImageWithURL:[NSURL URLWithString:[request.sender getProperty:@"profileImage"]]
                              placeholderImage:[UIImage imageNamed:@"profileImage"]
                                       options:SDWebImageRefreshCached];
        
    }
    else{
        [userImageView setImage:[UIImage imageNamed:@"profileImage"]];
    }

    
    
    
    [rootController layoutIfNeeded];
    
    
    return rootController ;
}


-(void)emailAction :(UIButton *)sender {
    Inbox *req = allNewRequests[sender.tag];
    NSString *URLEMail = [NSString stringWithFormat:@"mailto:%@?subject=RealKnu&body=Contact" , req.sender.email] ;
    NSString *url = [URLEMail stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding ];
    [[UIApplication sharedApplication]  openURL: [NSURL URLWithString: url]];
    
    
    
//    if (![MFMailComposeViewController canSendMail]) {
//        NSLog(@"Mail services are not available.");
//        return;
//    }
    
    
    
    
}
-(void)phoneAction :(UIButton *)sender {
    Inbox *req = allNewRequests[sender.tag];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@",[req.sender getProperty:@"phone"]]]];
}

-(void)whatsappAction :(UIButton *)sender {
    Inbox *req = allNewRequests[sender.tag];
    
    NSString *phoneNumber = [req.sender getProperty:@"phone"];
    if (![[phoneNumber substringToIndex:1] isEqualToString:@"1"]) {
        phoneNumber = [NSString stringWithFormat:@"1%@",phoneNumber];
    }
    

    
    
    NSURL *whatsappURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://api.whatsapp.com/send?phone=%@&text=Realknu....." , phoneNumber]];
    if ([[UIApplication sharedApplication] canOpenURL: whatsappURL]) {
        [[UIApplication sharedApplication] openURL: whatsappURL];
    }
}


-(void)closeAction :(UIButton *)sender {
    
    Inbox *req = allNewRequests[sender.tag];
    [allNewRequests removeObjectAtIndex:sender.tag];
    [self.popUpCarousel reloadData];
    req.status = @"contacted";
    id<IDataStore> dataStore = [backendless.persistenceService of:[Inbox class]];

    [dataStore save:req response:^(id req) {
        
    } error:^(Fault *error) {
        
    }];

}

-(void)GetCurrentLocation{
    locMgr = [INTULocationManager sharedInstance];
    [[CLLocationManager  new] requestAlwaysAuthorization];

    
    requestID = [locMgr subscribeToLocationUpdatesWithDesiredAccuracy:INTULocationAccuracyHouse block:^(CLLocation *currentLocation, INTULocationAccuracy achievedAccuracy, INTULocationStatus status) {
        
        
        if (status == INTULocationStatusSuccess) {
            
            
            double latitude = [[backendless.userService.currentUser getProperty:@"latitude"] floatValue];
            double longitude = [[backendless.userService.currentUser getProperty:@"longitude"] floatValue];

            
            CLLocation *serverLocation = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];

            
            CLLocationDistance distance = [serverLocation distanceFromLocation:currentLocation];
            
            if (distance > 200) {
                [self updateLocationOfUser:currentLocation];
            }
            
            
            
        }
        else {
            // An error occurred, more info is available by looking at the specific status returned. The subscription has been kept alive.
        }
    }];

    
}
-(void)updateLocationOfUser:(CLLocation *)loc{
    BackendlessUser *userToUpdate = backendless.userService.currentUser;
    
    if (userToUpdate == nil) {
        return;
    }
    
    [userToUpdate setProperty:@"longitude" object:[NSString stringWithFormat:@"%f",loc.coordinate.longitude]];
    [userToUpdate setProperty:@"latitude" object:[NSString stringWithFormat:@"%f",loc.coordinate.latitude]];

    
    id<IDataStore> dataStore1 = [backendless.persistenceService of:[BackendlessUser class]];
    [dataStore1 save:userToUpdate response:^(id cm) {
                NSLog(@"new Location saved ");
    } error:^(Fault *error) {
               NSLog(@"something went wrong... %@",error.detail);
        
    }];
}


-(void)reloadTable{
    isFirstTime = NO;
    NSLog(@"relaoded after one minit");
    [self fetchUsers];

}
-(void)maskTriananlge:(UIView *)view {
    // Build a triangular path
    [self.view layoutIfNeeded];
    UIBezierPath *path = [UIBezierPath new];
    [path moveToPoint:(CGPoint){view.frame.size.width / 2, 0}];
    
    [path addLineToPoint:(CGPoint){0, view.frame.size.height}];
    
    
    [path addLineToPoint:(CGPoint){view.frame.size.width, view.frame.size.height}];
    [path addLineToPoint:(CGPoint){view.frame.size.width / 2, 0}];
    
    // Create a CAShapeLayer with this triangular path
    // Same size as the original imageView
    CAShapeLayer *mask = [CAShapeLayer new];
    mask.frame = view.bounds;
    mask.path = path.CGPath;
    
    // Mask the imageView's layer with this shape
    view.layer.mask = mask;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)fetchUsers{
    
    NSUInteger previousCount = allNewRequests.count ;
    
    allAgentsArray = [[NSMutableArray alloc] init];
    allNewRequests = [[NSMutableArray alloc] init];
    if (isFirstTime) {
        [SVProgressHUD showWithStatus:@"Loading customers..."];

    }
    BackendlessDataQuery *query = [BackendlessDataQuery query];
    query.whereClause = [NSString stringWithFormat:@"sender.type = 'customer' AND receiver.objectId = '%@' AND status = 'pending'",backendless.userService.currentUser.objectId];
    id<IDataStore> dataStore = [backendless.persistenceService of:[Inbox class]];
    [dataStore find:query response:^(BackendlessCollection *allCustomer) {
        
        [SVProgressHUD dismiss];
        
        
        
        
        for (Inbox *inbox in allCustomer.data) {
            [allAgentsArray addObject:inbox];
            [allNewRequests addObject:inbox];
        }
        if (previousCount < allNewRequests.count) {

            

            
            [[TSMessageView appearance] setTitleFont:[UIFont boldSystemFontOfSize:18]];
            [[TSMessageView appearance] setContentFont:[UIFont boldSystemFontOfSize:15]];
            
            
            
            [TSMessage showNotificationInViewController:self
                                                  title:@"Contact Request Recieved"
                                               subtitle:@"You have recieved New Contact Request"
                                                  image:nil
                                                   type:TSMessageNotificationTypeMessage
                                               duration:TSMessageNotificationDurationAutomatic
                                               callback:nil
                                            buttonTitle:@"OK"
                                         buttonCallback:^{
                                             NSLog(@"User tapped the button");
                                         }
                                             atPosition:TSMessageNotificationPositionTop
                                   canBeDismissedByUser:YES];
        }
        [self.tableView reloadData];
        [self.popUpCarousel reloadData];
        
    } error:^(Fault *error) {
        [SVProgressHUD dismiss];
        NSLog(@"Error Found at photo  %@",error.detail);
    }];
    
    
}
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"listTCustomerProfile"]) {
        CustomerProfileViewController *vc = segue.destinationViewController ;
        vc.selectedUser = self.selectedUser;
    }
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return  allAgentsArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"userCell";
    
    UserCellTableViewCell  *cell = [self.tableView dequeueReusableCellWithIdentifier:MyIdentifier forIndexPath:indexPath];
    
    if (cell == nil)
    {
        cell = [[UserCellTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                            reuseIdentifier:MyIdentifier];
    }
    
    
    if(indexPath.row % 2 == 0){
        cell.backgroundColor = [UIColor whiteColor];
    }
    else
    {
        cell.backgroundColor = [UIColor colorWithRed:236/255.0 green:239/255.0 blue:236/255.0 alpha:1];
    }
    
    
    [self.view layoutIfNeeded];
    /*Inbox *inbox = [allAgentsArray objectAtIndex:indexPath.row];
    
    cell.phone.text = [inbox.sender getProperty:@"phone"];
    cell.name.text = inbox.sender.name;
    [cell.profileImag.layer setCornerRadius:cell.profileImag.frame.size.height/2];
    cell.profileImag.clipsToBounds=YES;
    cell.tag = indexPath.row;
    
    if (![[inbox.sender getProperty:@"profileImage"] isEqual:[NSNull null]]) {
        [cell.profileImag sd_setImageWithURL:[NSURL URLWithString:[backendless.userService.currentUser getProperty:@"profileImage"]]
                             placeholderImage:[UIImage imageNamed:@"avatar.png"]
                                      options:SDWebImageRefreshCached];
        
    }
    else{
        [cell.profileImag setImage:[UIImage imageNamed:@"avatar.png"]];
    }

    
    cell.rightUtilityButtons = [self rightButtons];
    cell.delegate = self;
     */
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    Inbox *inbox = allAgentsArray[indexPath.row];
    self.selectedUser = inbox.sender;
    [self performSegueWithIdentifier:@"listTCustomerProfile" sender:self];
    
}
- (NSArray *)rightButtons
{
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    [rightUtilityButtons sw_addUtilityButtonWithColor:
     [UIColor colorWithRed:25/255.0f green:227/255.0f blue:191/255.0f alpha:1.0f]
                                                title:@"Accept"];
    [rightUtilityButtons sw_addUtilityButtonWithColor:
     [UIColor colorWithRed:227/255.0f green:140/255.0f blue:148/255.0f alpha:1.0f]
                                                title:@"Reject"];
    
    return rightUtilityButtons;
}
- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index {
    switch (index) {
        case 0:
             [self updateStatus:@"accepted"andInbox:allAgentsArray[cell.tag]];
             [self.tableView reloadData];
             break;
        case 1:
             [self removeRejected:allAgentsArray[cell.tag]];
             isFirstTime = NO;
             [self fetchUsers];
            
             break;
       
        default:
            break;
    }
}
-(void)updateStatus:(NSString *)status andInbox:(Inbox *)inbox{
    
    inbox.status = status;
    id<IDataStore> dataStore1 = [backendless.persistenceService of:[Inbox class]];
    [dataStore1 save:inbox response:^(id cm) {
        NSLog(@"success");
        //request saved
    } error:^(Fault *error) {
       
        NSLog(@"something went wrong... %@",error.detail);
        
    }];
}
-(void)removeRejected:(Inbox *)inbox{
    
    id<IDataStore> dataStore1 = [backendless.persistenceService of:[Inbox class]];
    [dataStore1 remove:inbox response:^(id cm) {
        NSLog(@"success");
        //request saved
    } error:^(Fault *error) {
        
        NSLog(@"something went wrong... %@",error.detail);
        
    }];
}

-(IBAction)backAction:(UIButton *)sender {
    UIColor *colr = [UIColor colorWithRed:(255.0 / 255.0) green:(87.0 / 255.0) blue:(34.0 / 255.0) alpha:1.0];
    AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Logout?" andText:@"Are you sure, you want to Logout?" andCancelButton:YES forAlertType:AlertInfo andColor:colr withCompletionHandler:^(AMSmoothAlertView *alertView, UIButton *btton) {
        if(btton == alertView.defaultButton) {
            NSLog(@"Default");
            [alert dismissAlertView];
            [SVProgressHUD showWithStatus:@"Logging Out..."];
            if (backendless.userService.currentUser) {
                [backendless.userService.currentUser updateProperties:@{@"loginStatus" : @"loggedOff"}];
                [backendless.userService update:backendless.userService.currentUser response:^(BackendlessUser *newUSer) {
                    
                } error:^(Fault *error) {
                    NSLog(@"Error at the property update %@" , error.description);
                }];
            }
            [backendless.userService logout:^(id user) {
                [SVProgressHUD dismiss];
                NSLog(@"logged out");
                [self.navigationController popToRootViewControllerAnimated:YES];
                backendless.userService.currentUser = nil;
            } error:^(Fault *error) {
                [SVProgressHUD dismiss];
                NSLog(@"error at log out");
                [self.navigationController popToRootViewControllerAnimated:YES];
                backendless.userService.currentUser = nil;
            }
             ];
        }
        else {
            NSLog(@"Others");
            [alert dismissAlertView];
        }
    }];
    [alert.defaultButton setTitle:@"Yes" forState:UIControlStateNormal];
    [alert.defaultButton.titleLabel setFont: [alert.defaultButton.titleLabel.font fontWithSize: 18]];
    [alert.cancelButton setTitle:@"No" forState:UIControlStateNormal];
    [alert.cancelButton.titleLabel setFont: [alert.defaultButton.titleLabel.font fontWithSize: 18]];
    alert.cancelButton.backgroundColor = [UIColor grayColor];
    
    [alert setTitleFont:[UIFont systemFontOfSize:20]];
    [alert setTextFont:[UIFont systemFontOfSize:16]];

    
    [alert show];
}

- (IBAction)menuAction:(UIButton *)sender {
    if ([self.popUpView isHidden]) {
        [self.popUpView setHidden:NO];
        [self.triangleSpriteView setHidden:NO];
    }
    else
    {
        [self.popUpView setHidden:YES];
        [self.triangleSpriteView setHidden:YES];
    }
}


- (IBAction)unwindToContactList:(UIStoryboardSegue*)sender
{
    
}
@end
