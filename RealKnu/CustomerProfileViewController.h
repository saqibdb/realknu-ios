//
//  CustomerProfileViewController.h
//  RealKnu
//
//  Created by Shirley-Mac3 on 2/3/17.
//  Copyright © 2017 Jason Macbook. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Backendless.h"
@interface CustomerProfileViewController : UIViewController
- (IBAction)backAction:(UIButton *)sender;
- (IBAction)menueAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UILabel *userName;
@property (weak, nonatomic) IBOutlet UIImageView *profileImage;
@property (weak, nonatomic) IBOutlet UILabel *userNameSec;
@property (weak, nonatomic) IBOutlet UIView *popUpView;
@property (weak, nonatomic) IBOutlet UITextField *txtFirstName;
@property (weak, nonatomic) IBOutlet UITextField *txtLastName;
@property (strong) BackendlessUser *selectedUser;
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtPhone;
@property (weak, nonatomic) IBOutlet UIView *triangleSprite;

@end
