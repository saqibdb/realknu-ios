//
//  CustomerProfileViewController.m
//  RealKnu
//
//  Created by Shirley-Mac3 on 2/3/17.
//  Copyright © 2017 Jason Macbook. All rights reserved.
//

#import "CustomerProfileViewController.h"
#import "UIImageView+WebCache.h"
@interface CustomerProfileViewController ()

@end

@implementation CustomerProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view layoutIfNeeded];
    
    [self.popUpView setHidden:YES];
    [self.profileImage.layer setCornerRadius:self.profileImage.frame.size.height/2];
    self.profileImage.clipsToBounds=YES;
    
    [self.triangleSprite setHidden:YES];
    [self maskTriananlge:self.triangleSprite];
    
    self.userName.text = self.selectedUser.name;
    self.userNameSec.text = self.selectedUser.name;
    self.txtFirstName.text = [self.selectedUser getProperty:@"firstName"];
    self.txtLastName.text = [self.selectedUser getProperty:@"LastName"];
    self.txtEmail.text = [self.selectedUser getProperty:@"email"];
    self.txtPhone.text = [self.selectedUser getProperty:@"phone"];
    
    if (![[self.selectedUser getProperty:@"profileImage"] isEqual:[NSNull null]]) {
        [self.profileImage sd_setImageWithURL:[NSURL URLWithString:[self.selectedUser getProperty:@"profileImage"]]
                             placeholderImage:[UIImage imageNamed:@"avatar.png"]
                                      options:SDWebImageRefreshCached];
        
    }
    else{
        [self.profileImage setImage:[UIImage imageNamed:@"avatar.png"]];
    }

    // Do any additional setup after loading the view.
}
-(void)viewDidAppear:(BOOL)animated{

    [self.popUpView setHidden:YES];
    [self.triangleSprite setHidden:YES];

}
-(void)maskTriananlge:(UIView *)view {
    // Build a triangular path
    [self.view layoutIfNeeded];
    UIBezierPath *path = [UIBezierPath new];
    [path moveToPoint:(CGPoint){view.frame.size.width / 2, 0}];
    
    [path addLineToPoint:(CGPoint){0, view.frame.size.height}];
    
    
    [path addLineToPoint:(CGPoint){view.frame.size.width, view.frame.size.height}];
    [path addLineToPoint:(CGPoint){view.frame.size.width / 2, 0}];
    
    // Create a CAShapeLayer with this triangular path
    // Same size as the original imageView
    CAShapeLayer *mask = [CAShapeLayer new];
    mask.frame = view.bounds;
    mask.path = path.CGPath;
    
    // Mask the imageView's layer with this shape
    view.layer.mask = mask;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backAction:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (IBAction)menueAction:(UIButton *)sender {
    if ([self.popUpView isHidden]) {
        [self.popUpView setHidden:NO];
        [self.triangleSprite setHidden:NO];

    }
    else
    {
        [self.popUpView setHidden:YES];
        [self.triangleSprite setHidden:YES];

    }

}
@end
