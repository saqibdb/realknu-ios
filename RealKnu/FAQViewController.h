//
//  FAQViewController.h
//  RealKnu
//
//  Created by Shirley-Mac3 on 2/2/17.
//  Copyright © 2017 Jason Macbook. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FAQViewController : UIViewController
- (IBAction)backAction:(UIButton *)sender;
- (IBAction)menuAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIView *popUpView;
@property (weak, nonatomic) IBOutlet UIView *triangleSprite;


@end
