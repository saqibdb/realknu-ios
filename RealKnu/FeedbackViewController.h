//
//  FeedbackViewController.h
//  RealKnu
//
//  Created by Shirley-Mac3 on 2/7/17.
//  Copyright © 2017 Jason Macbook. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FRHyperLabel/FRHyperLabel.h>
@interface FeedbackViewController : UIViewController
- (IBAction)backeAction:(UIButton *)sender;
- (IBAction)menuAction:(UIButton *)sender;
    @property (weak, nonatomic) IBOutlet UITextView *textView;
- (IBAction)sendAction:(UIButton *)sender;
    @property (weak, nonatomic) IBOutlet UIView *popUp;
    @property (weak, nonatomic) IBOutlet UIView *triangle;
@property (weak, nonatomic) IBOutlet FRHyperLabel *feedbackText;
@property (weak, nonatomic) IBOutlet UILabel *feedbackHeading;

@end
