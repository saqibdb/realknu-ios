//
//  FeedbackViewController.m
//  RealKnu
//
//  Created by Shirley-Mac3 on 2/7/17.
//  Copyright © 2017 Jason Macbook. All rights reserved.
//
#import "Backendless.h"
#import "AMSmoothAlertView.h"
#import "SVProgressHUD.h"
#import "FeedbackViewController.h"
#import "SqbLogger.h"
#import "UITextView+Placeholder.h"

//@import UITextView_Placeholder;

@interface FeedbackViewController ()

@end

@implementation FeedbackViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view layoutIfNeeded];
    [self.view bringSubviewToFront:self.popUp];
    [self.popUp setHidden:YES];
    [self.triangle setHidden:YES];
    [self maskTriananlge:self.triangle];
    self.textView.placeholder = @"Your Message";
    self.textView.placeholderColor = [UIColor lightGrayColor];
    // Do any additional setup after loading the view.
    
    
    FRHyperLabel *label = self.feedbackText;
    label.numberOfLines = 0;
    
    //Step 1: Define a normal attributed string for non-link texts
    NSString *string = @"Have concerns, suggestions, ideas, we would love to hear from you, shoot us a note at:  ideas@RealKnu.com .  We promise to listen!";
    NSDictionary *attributes = @{NSForegroundColorAttributeName: [UIColor blackColor],NSFontAttributeName: [UIFont preferredFontForTextStyle:UIFontTextStyleHeadline]};
    label.attributedText = [[NSAttributedString alloc]initWithString:string attributes:attributes];
    
    label.font = self.feedbackHeading.font ;
    
    //Step 2: Define a selection handler block
    void(^handler)(FRHyperLabel *label, NSString *substring) = ^(FRHyperLabel *label, NSString *substring){
        /* UIAlertController *controller = [UIAlertController alertControllerWithTitle:substring message:nil preferredStyle:UIAlertControllerStyleAlert];
         [controller addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
         [self presentViewController:controller animated:YES completion:nil];
         */
        NSString *recipients = @"mailto:ideas@RealKnu.com?subject=Feedback Realknu";
        //NSString *body = @"&body=bodyHere";
        
        // NSString *email = [NSString stringWithFormat:@"%@%@", recipients, body];
        // email = [email stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:recipients]];
        
        
    };
    
    //Step 3: Add link substrings
    [label setLinksForSubstrings:@[@"ideas@RealKnu.com"] withLinkHandler:handler];
    
    
    
}
-(void)viewDidAppear:(BOOL)animated{

    [self.popUp setHidden:YES];
    [self.triangle setHidden:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
    -(void)maskTriananlge:(UIView *)view {
        // Build a triangular path
        [self.view layoutIfNeeded];
        UIBezierPath *path = [UIBezierPath new];
        [path moveToPoint:(CGPoint){view.frame.size.width / 2, 0}];
        [path addLineToPoint:(CGPoint){0, view.frame.size.height}];
        [path addLineToPoint:(CGPoint){view.frame.size.width, view.frame.size.height}];
        [path addLineToPoint:(CGPoint){view.frame.size.width / 2, 0}];
        
        CAShapeLayer *mask = [CAShapeLayer new];
        mask.frame = view.bounds;
        mask.path = path.CGPath;
        view.layer.mask = mask;
    }
    

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backeAction:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];

}
    
- (IBAction)menuAction:(UIButton *)sender {
    if ([self.popUp isHidden]) {
        [self.popUp setHidden:NO];
        [self.triangle setHidden:NO];
        
    }
    else
    {
        [self.popUp setHidden:YES];
        [self.triangle setHidden:YES];
     }

}
- (IBAction)sendAction:(UIButton *)sender {
    
    if (!self.textView.text.length) {
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"" andText:@"Message body cant't be empty" andCancelButton:NO forAlertType:AlertFailure];
        
        [alert show];
    }
    else
    {

    
    [SVProgressHUD show];
    NSString *subject = [NSString stringWithFormat:@"realKnu Feedback from %@ <%@> ",backendless.userService.currentUser.name,backendless.userService.currentUser.email ];
    NSString *body = self.textView.text;
    NSString *recipient = @"asadjavid46@gmail.com";
    [backendless.messagingService
     sendHTMLEmail:subject body:body to:@[recipient]
     response:^(id result) {
         AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"" andText:@"your Feedback sent to realKnu" andCancelButton:NO forAlertType:AlertSuccess];
         
         [alert show];
         [SVProgressHUD dismiss];
     }
     error:^(Fault *fault) {
         if ([fault.faultCode isEqualToString:@"-1009"]){
             [SVProgressHUD dismiss];
             AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Login Failed!!!" andText:@"The Internet connection appears to be offline." andCancelButton:NO forAlertType:AlertFailure];
             [alert show];
             return;
         }
         
         [SqbLogger logIt:[NSString stringWithFormat:@"%@ - %@" , fault.faultCode , fault.message ]] ;
         
         AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Oops!!!" andText:@"Something Unexpected has happened. Please Send Report to Developers." andCancelButton:YES forAlertType:AlertInfo andColor:[UIColor purpleColor] withCompletionHandler:^(AMSmoothAlertView *alertView, UIButton *btton) {
             if(btton == alertView.defaultButton) {
                 NSLog(@"Default");
                 [SqbLogger submitCrashReport:[NSString stringWithFormat:@"%@ - %@" , fault.faultCode , fault.message ]];
                 
                 [alert dismissAlertView];
                 
             }
             else {
                 NSLog(@"Others");
                 [alert dismissAlertView];
                 
             }
         }];
         [alert.defaultButton setTitle:@"Send Report" forState:UIControlStateNormal];
         [alert.defaultButton.titleLabel setFont: [alert.defaultButton.titleLabel.font fontWithSize: 12]];
         
         [alert.cancelButton setTitle:@"Close" forState:UIControlStateNormal];
         [alert.cancelButton.titleLabel setFont: [alert.defaultButton.titleLabel.font fontWithSize: 12]];
         
         
         [alert show];

         [SVProgressHUD dismiss];
     }];
    }
}
@end
