//
//  Inbox.h
//  RealKnu
//
//  Created by Shirley-Mac3 on 2/7/17.
//  Copyright © 2017 Jason Macbook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Backendless.h"
@interface Inbox : NSObject
    
    @property (nonatomic, strong) BackendlessUser *sender;
    @property (nonatomic, strong) BackendlessUser *receiver;
    @property (nonatomic, strong) NSString *status;
@end
