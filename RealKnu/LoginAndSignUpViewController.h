//
//  LoginAndSignUpViewController.h
//  RealKnu
//
//  Created by Shirley-Mac3 on 2/1/17.
//  Copyright © 2017 Jason Macbook. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginAndSignUpViewController : UIViewController<UIScrollViewDelegate , UITableViewDelegate , UITableViewDataSource>

- (IBAction)backAction:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UIView *signUpView;
@property (weak, nonatomic) IBOutlet UIView *loginView;
- (IBAction)showLoginAction:(UIButton *)sender;
- (IBAction)showSignUpAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIView *loginSprite;
@property (weak, nonatomic) IBOutlet UIView *signUpSprite;

//login
@property (weak, nonatomic) IBOutlet UITextField *txtUsername;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
@property (weak, nonatomic) IBOutlet UIButton *loginBtn;
- (IBAction)loginAction:(UIButton *)sender;
//signUp
@property (weak, nonatomic) IBOutlet UITextField *txtSusername;
@property (weak, nonatomic) IBOutlet UITextField *txtSpassword;
@property (weak, nonatomic) IBOutlet UITextField *txtSemail;
@property (weak, nonatomic) IBOutlet UIImageView *checkImage;
@property (weak, nonatomic) IBOutlet UIButton *signUpBtn;
- (IBAction)signUpAction:(UIButton *)sender;
- (IBAction)chekBtnAction:(UIButton *)sender;
- (IBAction)alreadyAprovedAction:(UIButton *)sender;
- (IBAction)openTermsAndConditionsPopUpAction:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UIView *loginBtnView;
@property (weak, nonatomic) IBOutlet UIView *signUpBtnView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIScrollView *subScrollView;



@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomConstraint;
@property (weak, nonatomic) IBOutlet UIButton *checkBtn;

@property (weak, nonatomic) IBOutlet UITableView *signUpTableView;





@end
