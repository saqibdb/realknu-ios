//
//  LoginAndSignUpViewController.m
//  RealKnu
//
//  Created by Shirley-Mac3 on 2/1/17.
//  Copyright © 2017 Jason Macbook. All rights reserved.
//

#import "LoginAndSignUpViewController.h"
#import "Backendless.h"
#import "AMSmoothAlertView.h"
#import "SVProgressHUD.h"
#import "TermsAndConditionsPopUp.h"
#import "UIViewController+CWPopup.h"
#import <INTULocationManager/INTULocationManager.h>
#import "SignUpLoginTableViewCell.h"
#import "SqbLogger.h"

@interface LoginAndSignUpViewController ()
{

    NSMutableArray *allUsersArray ;
}

@end

@implementation LoginAndSignUpViewController
- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [[self view] endEditing:YES];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self.loginView setHidden:NO];
    [self.loginSprite setHidden:NO];
    [self.signUpView setHidden:YES];
    [self.signUpSprite setHidden:YES];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
        UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    [self.view addGestureRecognizer:singleFingerTap];
    
    [self.view layoutIfNeeded];
    
    self.signUpTableView.dataSource = self;
    self.signUpTableView.delegate = self;
    
    if (backendless.userService.currentUser) {
        if ([[backendless.userService.currentUser getProperty:@"type"] isEqualToString:@"agent"]){
            [backendless.userService.currentUser setProperty:@"" object:@""];
            
            
            
            [self performSegueWithIdentifier:@"loginToList" sender:self];
        }
    }
}
-(void)viewDidAppear:(BOOL)animated{
   
    [self.signUpTableView reloadData];
}

- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer {

    [self.txtUsername resignFirstResponder];
    [self.txtPassword resignFirstResponder];
    [self.txtSusername resignFirstResponder];
    [self.txtSpassword resignFirstResponder];
    [self.txtSemail resignFirstResponder];
   

    //Do stuff here...
}


- (void)keyboardWillHide:(NSNotification *)notification
{
    self.bottomConstraint.constant = 0;
    [UIView animateWithDuration:0.2
                     animations:^{
                         [self.view layoutIfNeeded];
                     }];
}

- (void)keyboardWillShow:(NSNotification *)notification
{
    CGFloat height = [[notification.userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size.height;
    self.bottomConstraint.constant = height;
    [UIView animateWithDuration:0.2
                     animations:^{
                         [self.view layoutIfNeeded];
                     }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewDidLayoutSubviews
{
   [self.scrollView setContentSize:(CGSizeMake(self.subScrollView.frame.size.width, 1500))];
}


-(void)AcceptAction{
    self.checkBtn.tag = 0;
    [self.checkImage setImage:[UIImage imageNamed:@"checkIcon"]];

    [self dismissPopup];
}
-(void)dismissPopup{
    
    [self dismissPopupViewControllerAnimated:YES completion:nil];
}

- (IBAction)backAction:(UIButton *)sender {
    //[[NSUserDefaults standardUserDefaults] removePersistentDomainForName:[[NSBundle mainBundle] bundleIdentifier]];
    
    [self.navigationController popViewControllerAnimated:YES];

   //[self performSegueWithIdentifier:@"loginToViewcontroller" sender:self];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1 ;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"SignUpLoginTableViewCell";
    SignUpLoginTableViewCell *cell  = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    cell.backgroundColor = [UIColor clearColor];
    cell.lUserName.text = [SqbLogger getAgentUserName];
    cell.lPassword.text = [SqbLogger getAgentUPassword];
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];

    
    NSLog(@"Password is %@" , [defaults objectForKey:@"password"]);
    
    
        return cell ;
}




@end
