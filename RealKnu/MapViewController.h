//
//  MapViewController.h
//  RealKnu
//
//  Created by Shirley-Mac3 on 2/6/17.
//  Copyright © 2017 Jason Macbook. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FFAPSegmentedControl.h"


@import GoogleMaps;
@interface MapViewController : UIViewController<GMSMapViewDelegate,CLLocationManagerDelegate, FFAPSegmentedControlDelegate>
    @property (weak, nonatomic) IBOutlet GMSMapView *mapView;
    @property (weak, nonatomic) IBOutlet UIView *findingView;
    @property (nonatomic,retain) CLLocationManager *locationManager;
- (IBAction)backAction:(id)sender;
- (IBAction)menuAction:(UIButton *)sender;
    @property (weak, nonatomic) IBOutlet UIView *popUpView;
    @property (weak, nonatomic) IBOutlet UIView *triangleSprite;

@property (weak, nonatomic) IBOutlet UIButton *logoutBtn;


@property (weak, nonatomic) IBOutlet FFAPSegmentedControl *agentTypeSegment;


@property (strong) NSDictionary *selectedCity;



- (IBAction)refreshLocationAction:(UIButton *)sender;
- (IBAction)logoutAction:(id)sender;
- (IBAction)changeCityAction:(UIButton *)sender;
- (IBAction)myLocationAction:(UIButton *)sender;

@end
