//
//  MapViewController.m
//  RealKnu
//
//  Created by Shirley-Mac3 on 2/6/17.
//  Copyright © 2017 Jason Macbook. All rights reserved.
//
#import "UIImageView+WebCache.h"
#import "Inbox.h"
#import "ConnectPopUp.h"
#import "MapViewController.h"
#import "Backendless.h"
#import "AMSmoothAlertView.h"
#import "SVProgressHUD.h"
#import <INTULocationManager/INTULocationManager.h>
#import "UIViewController+CWPopup.h"
#import "SqbLogger.h"
#import "JDFTooltipView.h"
#import "XXXRoundMenuButton.h"



#define kSearchRadius = 20

@import GoogleMaps;

@interface MapViewController (){
    ConnectPopUp *connectPopUp;
    NSMutableArray *allAgentsArray;
    NSMutableArray *allInboxRequestUsers;
    CLLocation *location;
    BackendlessUser *receiverUser;
    CLLocation *closestLocation;
    BackendlessUser *selectedUser;
    GMSMarker *marker;
    NSTimer *timer ;
    GMSMarker *markerMe;
    GMSCircle *circ ;
    FFAPSegmentedControl *obj;
    JDFTooltipView *tooltip;
    NSString *selectedLanguage;
}
@property (weak, nonatomic) IBOutlet XXXRoundMenuButton *roundMenu;
@property (weak, nonatomic) IBOutlet XXXRoundMenuButton *roundMenu2;

@end
@implementation MapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    allAgentsArray = [[NSMutableArray alloc]init];
    [self.mapView setHidden:YES];
    self.mapView.delegate = self;
    self.mapView.myLocationEnabled = YES;
    self.mapView.settings.myLocationButton = NO;
    
    [self.view layoutIfNeeded];
    [self.popUpView setHidden:YES];
    [self.triangleSprite setHidden:YES];
    [self maskTriananlge:self.triangleSprite];
    
    

    timer = [NSTimer scheduledTimerWithTimeInterval:30.0 target:self selector:@selector(fetchAllAgentsAndFilterNearestAgent) userInfo:nil repeats:YES];
    
    

    

    [self setupRoundedMenus];
    
    // Do any additional setup after loading the view.
}


-(void)dealloc{
    [timer invalidate];
}

-(void)viewDidAppear:(BOOL)animated{
    
    [self.view layoutIfNeeded];
    
    CGFloat space = 0;
    
    
    if (obj) {
        [obj removeFromSuperview];
        obj = nil;
    }
    obj = [[FFAPSegmentedControl alloc] initWithFrame:CGRectMake(space,0,self.agentTypeSegment.frame.size.width,self.agentTypeSegment.frame.size.height)];
    obj.listOptions = @[@"RES", @"COM", @"RNTL",@"ALL"];
    obj.normalBackgroundColor = [UIColor colorWithRed:(41.0 / 255.0) green:(107.0 / 255.0) blue:(177.0 / 255.0) alpha:1.00];
    obj.normalTextColor = [UIColor whiteColor];
    obj.normalBorderColor = [UIColor clearColor];
    obj.selectedBackgroundColor = [UIColor colorWithRed:(31.0 / 255.0) green:(79.0 / 255.0) blue:(130.0 / 255.0) alpha:1.00];
    obj.selectedBorderColor = [UIColor whiteColor];
    obj.selectedImage = [UIImage imageNamed:@"checked.png"];
    obj.borderWidth = 2.0f;
    obj.selectedIndex = 3;
    obj.delegate = self;
    obj.spaceBetween = 10.0;
    [self.agentTypeSegment addSubview:obj];
    
    
    [self.popUpView setHidden:YES];
    [self.triangleSprite setHidden:YES];
    
    if (self.selectedCity) {
        [self gotoSelectedCityLocationWithCityDict:self.selectedCity];
    }
    else{
        [self GetCurrentLocation];
    }
    
    [self fetchInboxUserRequests];
    
}
-(void)viewWillDisappear:(BOOL)animated{
    //[timer invalidate];
}
#pragma mark - methods

-(void)maskTriananlge:(UIView *)view {
    // Build a triangular path
    [self.view layoutIfNeeded];
    UIBezierPath *path = [UIBezierPath new];
    [path moveToPoint:(CGPoint){view.frame.size.width / 2, 0}];
    [path addLineToPoint:(CGPoint){0, view.frame.size.height}];
    [path addLineToPoint:(CGPoint){view.frame.size.width, view.frame.size.height}];
    [path addLineToPoint:(CGPoint){view.frame.size.width / 2, 0}];
    
    CAShapeLayer *mask = [CAShapeLayer new];
    mask.frame = view.bounds;
    mask.path = path.CGPath;
    view.layer.mask = mask;
}



-(void)gotoSelectedCityLocationWithCityDict :(NSDictionary *)cityDict {
    [self.mapView clear];
    if (markerMe) {
        markerMe.map = nil;
        markerMe.icon = nil;
    }
    
    
    
    [self.view layoutIfNeeded];
    [self.mapView layoutIfNeeded];
    // A new updated location is available in currentLocation, and achievedAccuracy indicates how accurate this particular location is.
    //[self.mapView animateToCameraPosition:[GMSCameraPosition cameraWithLatitude:location.coordinate.latitude longitude:location.coordinate.longitude zoom:16]];
    //set marker of current location
    
    markerMe = [[GMSMarker alloc] init];
    
    CLLocationCoordinate2D position = CLLocationCoordinate2DMake([[cityDict objectForKey:@"lat"] floatValue], [[cityDict objectForKey:@"lng"] floatValue]);
    markerMe = [GMSMarker markerWithPosition:position];
    markerMe.title = [NSString stringWithFormat:@"%@",[cityDict objectForKey:@"QfistAndLastName"]];;
    markerMe.icon = [UIImage imageNamed:@"currentUserLocation"];
    markerMe.map = self.mapView;
    markerMe.userData = backendless.userService.currentUser;
    
    
    
    if (circ) {
        [circ setPosition:position];
        circ.map = self.mapView;
        
    }
    else{
        circ= [GMSCircle circleWithPosition:position
                                     radius:32188];
        
        circ.fillColor = [UIColor colorWithRed:0.25 green:0 blue:0 alpha:0.1];
        circ.strokeColor = [UIColor redColor];
        circ.strokeWidth = 1;
        circ.map = self.mapView;
        
    }
    
    
    
    [self.mapView animateToCameraPosition:[GMSCameraPosition cameraWithLatitude:position.latitude longitude:position.longitude zoom:16]];
    
    /*
    CLLocationDistance distance = [location distanceFromLocation:position];
    
    if (location == nil) {
        location = currentLocation;
        [self fetchAllAgentsAndFilterNearestAgent];
        [self.mapView animateToCameraPosition:[GMSCameraPosition cameraWithLatitude:location.coordinate.latitude
                                                                          longitude:location.coordinate.longitude
                                                                               zoom:16]];
        
        
        
        
        
    }
     */
}



-(void)GetCurrentLocation{
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    [[CLLocationManager  new] requestWhenInUseAuthorization];
    [locMgr subscribeToLocationUpdatesWithDesiredAccuracy:INTULocationAccuracyCity block:^(CLLocation *currentLocation, INTULocationAccuracy achievedAccuracy, INTULocationStatus status) {
        NSLog(@"NCurrent Location Updated");

        if (status == INTULocationStatusSuccess) {
            
            if (!self.selectedCity) {
                if (markerMe) {
                    markerMe.map = nil;
                    markerMe.icon = nil;
                }
                [self.view layoutIfNeeded];
                [self.mapView layoutIfNeeded];
                
                markerMe = [[GMSMarker alloc] init];
                
                CLLocationCoordinate2D position = CLLocationCoordinate2DMake(currentLocation.coordinate.latitude, currentLocation.coordinate.longitude);
                markerMe = [GMSMarker markerWithPosition:position];
                markerMe.title = @"You";
                markerMe.icon = [UIImage imageNamed:@"currentUserLocation"];
                markerMe.map = self.mapView;
                markerMe.userData = backendless.userService.currentUser;
                
                if (circ) {
                    [circ setPosition:currentLocation.coordinate];
                    circ.map = self.mapView;
                    
                }
                else{
                    circ= [GMSCircle circleWithPosition:currentLocation.coordinate
                                                 radius:32188];
                    circ.fillColor = [UIColor colorWithRed:0.25 green:0 blue:0 alpha:0.1];
                    circ.strokeColor = [UIColor redColor];
                    circ.strokeWidth = 1;
                    circ.map = self.mapView;
                }
            }
            
            CLLocationDistance distance = [location distanceFromLocation:currentLocation];
            if (location == nil) {
                location = currentLocation;
                [self fetchAllAgentsAndFilterNearestAgent];
                [self.mapView animateToCameraPosition:[GMSCameraPosition cameraWithLatitude:location.coordinate.latitude longitude:location.coordinate.longitude zoom:16]];

            }
            if (distance > 1000) {
                if (self.selectedCity) {

                    
                    
                }
                else{
                    location = currentLocation;
                    [self fetchAllAgentsAndFilterNearestAgent];
                    NSLog(@"Refreshed");
                }
                
                
                
            }
            else{
                NSLog(@"Not Refreshed");
            }
            
            
        }
        else {
            // An error occurred, more info is available by looking at the specific status returned. The subscription has been kept alive.
        }
    }];
    
    


}


-(void)fetchAllAgentsAndFilterNearestAgent{
    
    //[self.mapView clear];
    //NSMutableArray *allLocationCoordinatesOfAgents = [[NSMutableArray alloc]init];
    
    BackendlessDataQuery *query = [BackendlessDataQuery query];
    
    query.queryOptions.pageSize = @(100); //set page size
    
    
    
    NSString *querryString = [NSString stringWithFormat:@"Type = 'agent' AND approved = True AND loginStatus = 'loggedIn'"];
    if (obj.selectedIndex == 0) {
        querryString = [NSString stringWithFormat:@"Type = 'agent' AND approved = True AND agentType LIKE '%%RR%%' AND loginStatus = 'loggedIn'"];
    }
    else if (obj.selectedIndex == 1) {
        querryString = [NSString stringWithFormat:@"Type = 'agent' AND approved = True AND agentType LIKE '%%RC%%' AND loginStatus = 'loggedIn'"];
    }
    else if (obj.selectedIndex == 2) {
        querryString = [NSString stringWithFormat:@"Type = 'agent' AND approved = True AND agentType LIKE '%%Ar%%' AND loginStatus = 'loggedIn'"];
    }
    else{
        querryString = [NSString stringWithFormat:@"Type = 'agent' AND approved = True AND loginStatus = 'loggedIn'"];
    }
    if (selectedLanguage.length) {
        querryString = [NSString stringWithFormat:@"%@ AND Lenguage LIKE '%%%@%%'",querryString, selectedLanguage];
    }
    else{
        //querryString = [NSString stringWithFormat:@"%@ AND Lenguage LIKE '%%English%%'",querryString];
    }
    //querryString = [querryString stringByReplacingOccurrencesOfString:@" AND loginStatus = 'loggedIn'" withString:@""];
    
    NSLog(@"%@",querryString);
    query.whereClause = querryString;
    //query.whereClause = [NSString stringWithFormat:@"Type = 'agent' AND approved = True AND loginStatus = 'loggedIn'"];
    id<IDataStore> dataStore = [backendless.persistenceService of:[BackendlessUser class]];
    [dataStore find:query response:^(BackendlessCollection *allAgents) {
        
        [SVProgressHUD dismiss];
        
        [self.mapView clear];
        if (self.selectedCity) {
            if (markerMe) {
                markerMe.map = nil;
                markerMe.icon = nil;
            }
            
            
            
            [self.view layoutIfNeeded];
            [self.mapView layoutIfNeeded];
            // A new updated location is available in currentLocation, and achievedAccuracy indicates how accurate this particular location is.
            //[self.mapView animateToCameraPosition:[GMSCameraPosition cameraWithLatitude:location.coordinate.latitude longitude:location.coordinate.longitude zoom:16]];
            //set marker of current location
            
            markerMe = [[GMSMarker alloc] init];
            
            CLLocationCoordinate2D position = CLLocationCoordinate2DMake([[self.selectedCity objectForKey:@"lat"] floatValue], [[self.selectedCity objectForKey:@"lng"] floatValue]);
            markerMe = [GMSMarker markerWithPosition:position];
            markerMe.title = [NSString stringWithFormat:@"%@",[self.selectedCity objectForKey:@"QfistAndLastName"]];
            markerMe.icon = [UIImage imageNamed:@"currentUserLocation"];
            markerMe.map = self.mapView;
            markerMe.userData = backendless.userService.currentUser;
            
            
            
            if (circ) {
                [circ setPosition:position];
                circ.map = self.mapView;
                
            }
            else{
                circ= [GMSCircle circleWithPosition:position
                                             radius:32188];
                
                circ.fillColor = [UIColor colorWithRed:0.25 green:0 blue:0 alpha:0.1];
                circ.strokeColor = [UIColor redColor];
                circ.strokeWidth = 1;
                circ.map = self.mapView;
                
            }
            
            
            
            ///[self.mapView animateToCameraPosition:[GMSCameraPosition cameraWithLatitude:position.latitude longitude:position.longitude zoom:16]];
            
        }
        
        
        allAgentsArray = [[NSMutableArray alloc] initWithArray:allAgents.data];
        NSLog(@"%lu agents found",(unsigned long)allAgentsArray.count);

        
        [self selectedUser:allAgentsArray];

        
        
        
        
        
        [self.mapView setHidden:NO];
        [self.findingView setHidden:YES];
        
        
    } error:^(Fault *error) {
        [SVProgressHUD dismiss];
        NSLog(@"Error Found at photo%@",error.detail);
    }];
}

-(void)selectedUser:(NSMutableArray *)agentArray {
    
    NSString *latitude;
    NSString *longitude ;
    //set camera to current location
    
    CLLocationDistance bestDistance = 32188;
    for (BackendlessUser *user in allAgentsArray) {
        
        latitude = [NSString stringWithFormat:@"%@",[user getProperty:@"latitude"]];
        longitude = [NSString stringWithFormat:@"%@",[user getProperty:@"longitude"]];
        
        CLLocation *LocationAtual = [[CLLocation alloc] initWithLatitude:[latitude floatValue] longitude:[longitude floatValue]];
        
        
        CLLocation *LocationToSearch ;
        
        if (self.selectedCity) {
            LocationToSearch = [[CLLocation alloc] initWithLatitude:[[self.selectedCity objectForKey:@"lat"] floatValue] longitude:[[self.selectedCity objectForKey:@"lng"] floatValue]];
        }
        else{
            LocationToSearch = location;
        }
        
        
        
        //if (bestDistance > [location distanceFromLocation:LocationAtual] && [location distanceFromLocation:LocationAtual] < 32187) {
        if ([LocationToSearch distanceFromLocation:LocationAtual] < 32188) {
            bestDistance = [location distanceFromLocation:LocationAtual];
            selectedUser = user;
            NSString *selectedUser_Latitude;
            NSString *selectedUser_Longitude;
            selectedUser_Latitude = [selectedUser getProperty:@"latitude"];
            selectedUser_Longitude = [selectedUser getProperty:@"longitude"];
        
            
            
            
            
            
            double lat ;
            double lan ;
            if (LocationToSearch.coordinate.latitude == [selectedUser_Latitude doubleValue]) {
                lat = [selectedUser_Latitude doubleValue] + (0.0001);
            }
            else{
                lat = [selectedUser_Latitude doubleValue];
                
            }
            if (LocationToSearch.coordinate.longitude == [selectedUser_Longitude doubleValue]) {
                lan = [selectedUser_Longitude doubleValue] + (0.0001);
            }
            else{
                lan = [selectedUser_Longitude doubleValue];
            }
            CLLocationCoordinate2D position =  (CLLocationCoordinate2D){.latitude = lat, .longitude = lan};
            GMSMarker *markerAgent = [GMSMarker markerWithPosition:position];
            //marker.title = selectedUser.name;
            
            UIImage *markerImage = [UIImage imageNamed:@"agentsArrondLocationRCR"];
            if ([[user getProperty:@"agentType"] isEqual:[NSNull null]]) {
                
            }
            else{
                
                if (obj.selectedIndex == 0) {
                    markerImage = [UIImage imageNamed:@"agentsArrondLocationRR"];
                }
                else if (obj.selectedIndex == 1) {
                    markerImage = [UIImage imageNamed:@"agentsArrondLocationRC"];
                }
                else if (obj.selectedIndex == 2) {
                    markerImage = [UIImage imageNamed:@"agentsArrondLocationAr"];
                }
                else{
                    if ([[user getProperty:@"agentType"] isEqualToString:@"RR"]) {
                        markerImage = [UIImage imageNamed:@"agentsArrondLocationRR"];
                    }
                    else if ([[user getProperty:@"agentType"] isEqualToString:@"RC"]) {
                        markerImage = [UIImage imageNamed:@"agentsArrondLocationRC"];
                    }
                    else if ([[user getProperty:@"agentType"] isEqualToString:@"Ar"]) {
                        markerImage = [UIImage imageNamed:@"agentsArrondLocationAr"];
                    }
                    else {
                        markerImage = [UIImage imageNamed:@"agentsArrondLocationRCR"];
                    }
                }
                
                
                
                
            }
            markerAgent.icon = [UIImage imageNamed:@"newRLogo"];
            //markerAgent.icon = markerImage;
            markerAgent.map = self.mapView;
            markerAgent.userData = selectedUser;
        }
        else{
            //NSLog(@"Distance is %f" , [LocationToSearch distanceFromLocation:LocationAtual]) ;
        }
    }
    
    
    if (!selectedUser) {
        /*
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"No Agent Found !" andText:@"There is no agent around your current Location." andCancelButton:NO forAlertType:AlertSuccess];
        
        [alert show];
        */
        return ;
    }
    
    
    
    
}

-(void)fetchInboxUserRequests{
    allInboxRequestUsers = [[NSMutableArray alloc] init];
    BackendlessDataQuery *query = [BackendlessDataQuery query];
    query.whereClause = [NSString stringWithFormat:@"sender.objectId = '%@'",backendless.userService.currentUser.objectId];
    id<IDataStore> dataStore = [backendless.persistenceService of:[Inbox class]];
    [dataStore find:query response:^(BackendlessCollection *allCustomer) {
        
        [SVProgressHUD dismiss];
        
        for (Inbox *inbox in allCustomer.data) {
            [allInboxRequestUsers addObject:inbox];
        }
        
    } error:^(Fault *error) {
        [SVProgressHUD dismiss];
        NSLog(@"Error Found at photo  %@",error.detail);
    }];
}

- (BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker {
    
    receiverUser = marker.userData;
    
    
    for (Inbox *inbox in allInboxRequestUsers) {
        if ([inbox.receiver.objectId isEqualToString:receiverUser.objectId] && [inbox.status isEqualToString:@"pending"]) {
            
        }
    }
    
    if([receiverUser.objectId isEqualToString:backendless.userService.currentUser.objectId]){
        NSLog(@"cant connect you with yourself");
    
        return YES;
        
    }
    else{
        
        
        
        connectPopUp = [[ConnectPopUp alloc] initWithNibName:@"ConnectPopUp" bundle:nil];
        
        if (self.view.frame.size.width == 320) {
            connectPopUp.view.frame = CGRectMake(connectPopUp.view.frame.origin.x, connectPopUp.view.frame.origin.y, 300, connectPopUp.view.frame.size.height);
        }
        
        
        [self presentPopupViewController:connectPopUp animated:YES completion:^(void) {
            [self.view layoutIfNeeded];
            
            
            [connectPopUp.declineBtn addTarget:self action:@selector(ConnectDeclineAction) forControlEvents:UIControlEventTouchUpInside];
            [connectPopUp.noBtn addTarget:self action:@selector(dismissPopup) forControlEvents:UIControlEventTouchUpInside];
            [connectPopUp.btnPhone addTarget:self action:@selector(phoneAction) forControlEvents:UIControlEventTouchUpInside];
            
            [connectPopUp.smsBtn addTarget:self action:@selector(smsAction) forControlEvents:UIControlEventTouchUpInside];
            [connectPopUp.callBtn addTarget:self action:@selector(callAction) forControlEvents:UIControlEventTouchUpInside];
            [connectPopUp.whatsAppBtn addTarget:self action:@selector(whatsAppAction) forControlEvents:UIControlEventTouchUpInside];

            
            
            
            if (allInboxRequestUsers.count>0) {
                for (Inbox *inbox in allInboxRequestUsers) {
                    if ([inbox.receiver.objectId isEqualToString:receiverUser.objectId] && [inbox.status isEqualToString:@"pending"]) {
                        connectPopUp.lblTitleText.text = [NSString stringWithFormat:@"Agent has been notified and will contact you shortly"];
                        
                        connectPopUp.yesBtn.enabled = NO;
                        [connectPopUp.yesBtn setTitle:@"Pending" forState:UIControlStateNormal];
                        [connectPopUp.yesBtn setBackgroundColor:[UIColor lightGrayColor]];
                        
                    }
                    else if ([inbox.receiver.objectId isEqualToString:receiverUser.objectId] && [inbox.status isEqualToString:@"accepted"]){
                        
                        connectPopUp.lblTitleText.text = [NSString stringWithFormat:@"Agent has been notified and will contact you shortly"];
                        connectPopUp.yesBtn.enabled = NO;
                        [connectPopUp.yesBtn setTitle:@"Accepted" forState:UIControlStateNormal];
                        [connectPopUp.yesBtn setBackgroundColor:[UIColor lightGrayColor]];
                        
                    }
                    else{
                        
                        connectPopUp.lblTitleText.text = [NSString stringWithFormat:@"To Busy Now?  Hit ACCEPT to Have Agent Contact You Directly"];
                        [connectPopUp.yesBtn addTarget:self action:@selector(ConnectYesAction) forControlEvents:UIControlEventTouchUpInside];
                        
                    }
                }
                
            }
            else{
                connectPopUp.lblTitleText.text = [NSString stringWithFormat:@"To Busy Now?  Hit ACCEPT to Have Agent Contact You Directly"];
                [connectPopUp.yesBtn addTarget:self action:@selector(ConnectYesAction) forControlEvents:UIControlEventTouchUpInside];
                
            }
            
            [connectPopUp.noBtn.layer setCornerRadius:connectPopUp.noBtn.frame.size.height/2];
            connectPopUp.noBtn.clipsToBounds=YES;
            connectPopUp.lblName.text = [receiverUser getProperty:@"QfistAndLastName"];
            connectPopUp.lblEmail.text =[NSString stringWithFormat:@"Email:%@",receiverUser.email] ;
            connectPopUp.lblPhone.text = [NSString stringWithFormat:@"Phone:%@",[receiverUser getProperty:@"bestPhoneNo"]];
            //
            connectPopUp.companyName.text = [NSString stringWithFormat:@"Company: %@",[receiverUser getProperty:@"companyName"]];
            connectPopUp.licenceNo.text = [NSString stringWithFormat:@"Licence No: %@",[receiverUser getProperty:@"licenceNumber"]];
            connectPopUp.speciality.text = [NSString stringWithFormat:@"Speciality: %@",[receiverUser getProperty:@"speciality"]];
            connectPopUp.lenguage.text = [NSString stringWithFormat:@"Language: %@",[receiverUser getProperty:@"Lenguage"]];
            
            if (![[receiverUser getProperty:@"profileImage"] isEqual:[NSNull null]]) {
                [connectPopUp.image sd_setImageWithURL:[NSURL URLWithString:[receiverUser getProperty:@"profileImage"]]
                                      placeholderImage:[UIImage imageNamed:@"avatar.png"]
                                               options:SDWebImageRefreshCached];
                
            }
            else{
                [connectPopUp.image setImage:[UIImage imageNamed:@"avatar.png"]];
            }
            
        }];
        return YES;
    }
}
-(void)dismissPopup{
    
    [self dismissPopupViewControllerAnimated:YES completion:nil];
}


-(void)ConnectYesAction{
    Inbox *inbox = [Inbox new];
    
    inbox.sender = backendless.userService.currentUser;
    inbox.receiver= receiverUser ;
    inbox.status = @"pending";
    
    id<IDataStore> dataStore1 = [backendless.persistenceService of:[Inbox class]];
    [dataStore1 save:inbox response:^(id cm) {
        [self dismissPopup];
        
        
        [self fetchInboxUserRequests ];
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"" andText:@"Connect Request Sent!" andCancelButton:NO forAlertType:AlertSuccess];
        
        [alert show];
        //request saved
    } error:^(Fault *error) {
        
        [self dismissPopup];
        
        if ([error.faultCode isEqualToString:@"-1009"]){
            [SVProgressHUD dismiss];
            AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Login Failed!!!" andText:@"The Internet connection appears to be offline." andCancelButton:NO forAlertType:AlertFailure];
            [alert show];
            return;
        }
        
        
        [SqbLogger logIt:[NSString stringWithFormat:@"%@ - %@" , error.faultCode , error.message ]] ;
        
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Oops!!!" andText:@"Something Unexpected has happened. Please Send Report to Developers." andCancelButton:YES forAlertType:AlertInfo andColor:[UIColor purpleColor] withCompletionHandler:^(AMSmoothAlertView *alertView, UIButton *btton) {
            if(btton == alertView.defaultButton) {
                NSLog(@"Default");
                [SqbLogger submitCrashReport:[NSString stringWithFormat:@"%@ - %@" , error.faultCode , error.message ]];
                
                [alert dismissAlertView];
                
            }
            else {
                NSLog(@"Others");
                [alert dismissAlertView];
                
            }
        }];
        [alert.defaultButton setTitle:@"Send Report" forState:UIControlStateNormal];
        [alert.defaultButton.titleLabel setFont: [alert.defaultButton.titleLabel.font fontWithSize: 12]];
        
        [alert.cancelButton setTitle:@"Close" forState:UIControlStateNormal];
        [alert.cancelButton.titleLabel setFont: [alert.defaultButton.titleLabel.font fontWithSize: 12]];
        
        
        [alert show];
        NSLog(@"something went wrong... %@",error.detail);
        
    }];
}

-(void)ConnectDeclineAction{
    [SVProgressHUD show];
    
    Inbox *inbox = [Inbox new];
    
    inbox.sender = backendless.userService.currentUser;
    inbox.receiver= receiverUser ;
    inbox.status = @"rejected";
    
    id<IDataStore> dataStore1 = [backendless.persistenceService of:[Inbox class]];
    [dataStore1 save:inbox response:^(id cm) {
        [self dismissPopup];
        [SVProgressHUD dismiss];

        marker.map = nil;
        [allAgentsArray removeObject:selectedUser];
        selectedUser = nil;
        [self selectedUser:allAgentsArray];
        
            //do rejection stuff here
    } error:^(Fault *error) {
        [SVProgressHUD dismiss];
        [self dismissPopup];
        
        if ([error.faultCode isEqualToString:@"-1009"]){
            [SVProgressHUD dismiss];
            AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Login Failed!!!" andText:@"The Internet connection appears to be offline." andCancelButton:NO forAlertType:AlertFailure];
            [alert show];
            return;
        }
        
        [SqbLogger logIt:[NSString stringWithFormat:@"%@ - %@" , error.faultCode , error.message ]] ;
        
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Oops!!!" andText:@"Something Unexpected has happened. Please Send Report to Developers." andCancelButton:YES forAlertType:AlertInfo andColor:[UIColor purpleColor] withCompletionHandler:^(AMSmoothAlertView *alertView, UIButton *btton) {
            if(btton == alertView.defaultButton) {
                NSLog(@"Default");
                [SqbLogger submitCrashReport:[NSString stringWithFormat:@"%@ - %@" , error.faultCode , error.message ]];
                
                [alert dismissAlertView];
                
            }
            else {
                NSLog(@"Others");
                [alert dismissAlertView];
                
            }
        }];
        [alert.defaultButton setTitle:@"Send Report" forState:UIControlStateNormal];
        [alert.defaultButton.titleLabel setFont: [alert.defaultButton.titleLabel.font fontWithSize: 12]];
        
        [alert.cancelButton setTitle:@"Close" forState:UIControlStateNormal];
        [alert.cancelButton.titleLabel setFont: [alert.defaultButton.titleLabel.font fontWithSize: 12]];
        
        
        [alert show];

        NSLog(@"something went wrong... %@",error.detail);
        
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
#pragma mark - Actions

-(void)phoneAction{
    UIColor *colr = [UIColor colorWithRed:41/255.0 green:107/255.0 blue:177/255.0 alpha:1];
    
    AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Alert!" andText:@"How Do you want to contact this Agent?" andCancelButton:YES forAlertType:AlertInfo andColor:colr withCompletionHandler:^(AMSmoothAlertView *alertView, UIButton *btton) {
        if(btton == alertView.defaultButton) {
            NSLog(@"Default");
            [alert dismissAlertView];
            NSLog(@"calling Phone No: %@", connectPopUp.lblPhone.text);
            NSString *phoneNo = [@"tel://" stringByAppendingString:[NSString stringWithFormat:@"%@",[receiverUser getProperty:@"bestPhoneNo"]]];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNo]];
        }
        else {
            
            if (btton == alertView.nCloseBtn) {
                NSLog(@"Close");
            }
            else{
                NSLog(@"SMS");
                
                NSString *phoneNo = [@"sms://" stringByAppendingString:[NSString stringWithFormat:@"%@",[receiverUser getProperty:@"bestPhoneNo"]]];
                
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNo]];
            }
            [alert dismissAlertView];
        }
    }];
    [alert.defaultButton setTitle:@"Phone" forState:UIControlStateNormal];
    [alert.defaultButton.titleLabel setFont: [alert.defaultButton.titleLabel.font fontWithSize: 12]];
    
    [alert.cancelButton setTitle:@"SMS" forState:UIControlStateNormal];
    [alert.cancelButton.titleLabel setFont: [alert.defaultButton.titleLabel.font fontWithSize: 12]];
    [alert.cancelButton setBackgroundColor:[UIColor colorWithRed:(30.0/255.0) green:(130.0/255.0) blue:(76.0/255.0) alpha:1.0]];
    [alert.nCloseBtn setImage:[UIImage imageNamed:@"closeIcon"] forState:UIControlStateNormal] ;
    [alert.nCloseBtn setImageEdgeInsets:UIEdgeInsetsMake(6, 6, 6, 6)];
    
    alert.nCloseBtn.hidden = NO;
    
    [alert show];
}

-(void)smsAction{
    NSLog(@"SMS");
    
    NSString *phoneNo = [@"sms://" stringByAppendingString:[NSString stringWithFormat:@"%@",[receiverUser getProperty:@"bestPhoneNo"]]];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNo]];
}



-(void)callAction{
    NSLog(@"calling Phone No: %@", connectPopUp.lblPhone.text);
    NSString *phoneNo = [@"tel://" stringByAppendingString:[NSString stringWithFormat:@"%@",[receiverUser getProperty:@"bestPhoneNo"]]];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNo]];
}

-(void)whatsAppAction{
    
    NSString *phoneNumber = [receiverUser getProperty:@"bestPhoneNo"];
    if (![[phoneNumber substringToIndex:1] isEqualToString:@"1"]) {
        phoneNumber = [NSString stringWithFormat:@"1%@",phoneNumber];
    }
    
    NSString *phoneNo = [@"tel://" stringByAppendingString:[NSString stringWithFormat:@"%@",phoneNumber]];

    NSURL *whatsappURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://api.whatsapp.com/send?phone=%@&text=Realknu....." , phoneNo]];
    if ([[UIApplication sharedApplication] canOpenURL: whatsappURL]) {
        [[UIApplication sharedApplication] openURL: whatsappURL];
    }
}



- (IBAction)backAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (IBAction)menuAction:(UIButton *)sender {
    [self.view layoutIfNeeded];
    
    if ([self.popUpView isHidden]) {
        [self.popUpView setHidden:NO];
        [self.triangleSprite setHidden:NO];
    }
    else
    {
        [self.popUpView setHidden:YES];
        [self.triangleSprite setHidden:YES];
    }
    
    
}
- (IBAction)refreshLocationAction:(UIButton *)sender {
    [self GetCurrentLocation];
}

- (IBAction)logoutAction:(id)sender {
    
    //rgb(255, 87, 34)
    
    UIColor *colr = [UIColor colorWithRed:(255.0 / 255.0) green:(87.0 / 255.0) blue:(34.0 / 255.0) alpha:1.0];
    
    
    AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Logout?" andText:@"Are you sure, you want to Logout?" andCancelButton:YES forAlertType:AlertInfo andColor:colr withCompletionHandler:^(AMSmoothAlertView *alertView, UIButton *btton) {
        if(btton == alertView.defaultButton) {
            NSLog(@"Default");
            
            [alert dismissAlertView];
            
            [SVProgressHUD showWithStatus:@"Logging Out..."];
            
            
            if (backendless.userService.currentUser) {
                [backendless.userService.currentUser updateProperties:@{@"loginStatus" : @"loggedOff"}];
                [backendless.userService update:backendless.userService.currentUser response:^(BackendlessUser *newUSer) {

                } error:^(Fault *error) {
                    NSLog(@"Error at the property update %@" , error.description);
                }];
            }
            
            
            [backendless.userService logout:^(id user) {
                [SVProgressHUD dismiss];
                NSLog(@"logged out");
                [self.navigationController popToRootViewControllerAnimated:YES];
                backendless.userService.currentUser = nil;
                
            } error:^(Fault *error) {
                [SVProgressHUD dismiss];
                NSLog(@"error at log out");
                [self.navigationController popToRootViewControllerAnimated:YES];
                backendless.userService.currentUser = nil;
            }
             ]; 
        }
        else {
            NSLog(@"Others");
            [alert dismissAlertView];
            
        }
    }];
    [alert.defaultButton setTitle:@"Yes" forState:UIControlStateNormal];
    [alert.defaultButton.titleLabel setFont: [alert.defaultButton.titleLabel.font fontWithSize: 18]];
    [alert.cancelButton setTitle:@"No" forState:UIControlStateNormal];
    [alert.cancelButton.titleLabel setFont: [alert.defaultButton.titleLabel.font fontWithSize: 18]];
    
    [alert setTitleFont:[UIFont systemFontOfSize:20]];
    [alert setTextFont:[UIFont systemFontOfSize:16]];
    alert.cancelButton.backgroundColor = [UIColor grayColor];
    
    [alert show];
}

- (IBAction)changeCityAction:(UIButton *)sender {
    
    [self performSegueWithIdentifier:@"ToChangeCity" sender:self];
}

- (IBAction)myLocationAction:(UIButton *)sender {
    self.selectedCity = nil;
    CLLocation *locationMine = self.mapView.myLocation;
    if (locationMine) {
        [self.mapView animateToLocation:locationMine.coordinate];
    }
    [self GetCurrentLocation];
}




- (void) segmentDidChangeTheSelectedIndex: (id) sender{
    if (tooltip) {
        [tooltip hideAnimated:YES];
    }
    [self.mapView clear];
    FFAPSegmentedControl *aSegmentObj = (FFAPSegmentedControl *)sender;
    NSLog(@"Called Delegate at Index %i" , aSegmentObj.selectedIndex);
    [SVProgressHUD showWithStatus:@"Filtering Agents"];
    [self fetchAllAgentsAndFilterNearestAgent];
}
- (void) questionbuttonClickedAtIndex: (id) sender{
    UIButton *questionButton = (UIButton *)sender;
    
    NSLog(@"Question Clicked At %li" , (long)questionButton.tag);
    
    NSString *toolTipTitle ;
    
    
    if (tooltip) {
        [tooltip hideAnimated:YES];
        if (tooltip.tag == questionButton.tag) {
            tooltip.tag = 100;
            return;
        }
    }
    if (questionButton.tag == 0) {
        toolTipTitle = @"Agent Realtor";
    }
    else if (questionButton.tag == 1) {
        toolTipTitle = @"Agent Commercial";
    }
    else if (questionButton.tag == 2) {
        toolTipTitle = @"Agent Rental";
    }
    else if (questionButton.tag == 3) {
        toolTipTitle = @"Agent Residential, Commercial, Rental";
    }
    
    /*
     AR = Agent Realtor
     AC = Agent Commercial
     Ar = Agent Rental
     ACRr = Agent Residential, Commercial, Rental
     */
    
    
    
    tooltip = [[JDFTooltipView alloc] initWithTargetView:questionButton hostView:self.view tooltipText:toolTipTitle arrowDirection:JDFTooltipViewArrowDirectionUp width:200.0f];
    tooltip.tag = questionButton.tag;
    [tooltip show];
}

-(void)setupRoundedMenus {
   
    
    
    /**
     *  RoundMenu2 config
     */
    
    NSArray *countriesBtn = @[
                             [UIImage imageNamed:@"flag_france"],
                             [UIImage imageNamed:@"flag_usa"],
                             [UIImage imageNamed:@"flag_portugal"],
                             [UIImage imageNamed:@"flag_spain"],
                             [UIImage imageNamed:@"flag_italy"],
                             [UIImage imageNamed:@"flag_russia"]
                             ];
    NSArray *countriesStr = @[@"French", @"English",@"Portugues",@"Spanish",@"Italian",@"Russian"];

    [self.roundMenu2 loadButtonWithIcons:@[
                                           [UIImage imageNamed:@"flag_france"],
                                           [UIImage imageNamed:@"flag_usa"],
                                           [UIImage imageNamed:@"flag_portugal"],
                                           [UIImage imageNamed:@"flag_spain"],
                                           [UIImage imageNamed:@"flag_italy"],
                                           [UIImage imageNamed:@"flag_russia"]
                                           
                                           ] startDegree:M_PI layoutDegree:-M_PI];
    [self.roundMenu2 setButtonClickBlock:^(NSInteger idx) {
        
        NSLog(@"button %@ clicked !",@(idx));
        [self.roundMenu2 setCenterIcon:countriesBtn[idx]];
        
        selectedLanguage = countriesStr[idx];
        [self.mapView clear];
        
        
        [SVProgressHUD showWithStatus:@"Filtering Agents"];
        [self fetchAllAgentsAndFilterNearestAgent];
        

    }];
    
    [self.roundMenu2 setCenterIcon:[UIImage imageNamed:@"flag_usa"]];
    [self.roundMenu2 setCenterIconType:XXXIconTypeCustomImage];
    
    self.roundMenu2.tintColor = [UIColor whiteColor];
    
    self.roundMenu2.mainColor = [UIColor colorWithRed:(41.0/255.0) green:(107.0/255.0) blue:(177.0/255.0) alpha:1];
    
    self.roundMenu2.offsetAfterOpened = CGSizeMake(80, -80);
}
@end
