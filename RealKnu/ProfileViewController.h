//
//  ProfileViewController.h
//  RealKnu
//
//  Created by Shirley-Mac3 on 2/2/17.
//  Copyright © 2017 Jason Macbook. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Backendless.h"
@interface ProfileViewController : UIViewController<UIImagePickerControllerDelegate, UINavigationControllerDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *profileImage;
@property (weak, nonatomic) IBOutlet UILabel *userName;
@property (weak, nonatomic) IBOutlet UILabel *userNameSec;

@property (weak, nonatomic) IBOutlet UITextField *txtFirstName;
@property (weak, nonatomic) IBOutlet UITextField *txtLastName;
@property (weak, nonatomic) IBOutlet UITextField *email;
@property (weak, nonatomic) IBOutlet UITextField *phone;
//

@property (weak, nonatomic) IBOutlet UITextField *txtCompany;
@property (weak, nonatomic) IBOutlet UITextField *txtLicenceNo;
@property (weak, nonatomic) IBOutlet UITextField *txtSpeciality;
@property (weak, nonatomic) IBOutlet UITextField *txtLenguage;
//


- (IBAction)editInformationAction:(UIButton *)sender;



@property (weak, nonatomic) IBOutlet UIButton *editFirstNameBtn;
@property (weak, nonatomic) IBOutlet UIButton *editLastNameBtn;
@property (weak, nonatomic) IBOutlet UIButton *editEmailBtn;
@property (weak, nonatomic) IBOutlet UIButton *editPhoneBtn;
- (IBAction)backAction:(UIButton *)sender;
- (IBAction)menueAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIView *popUpView;
@property (weak, nonatomic) IBOutlet UIView *triangleSprite;
- (IBAction)changeProfileImageAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *ChangeProfileBtn;
- (IBAction)savaChangesAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIView *agentSpecificInfoView;

@end
