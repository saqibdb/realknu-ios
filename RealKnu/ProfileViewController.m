//
//  ProfileViewController.m
//  RealKnu
//
//  Created by Shirley-Mac3 on 2/2/17.
//  Copyright © 2017 Jason Macbook. All rights reserved.
//
#import "UIImageView+WebCache.h"
#import "ProfileViewController.h"





#import "ActionSheetPicker.h"
#import "AMSmoothAlertView.h"
#import "SVProgressHUD.h"
#import "SqbLogger.h"



@interface ProfileViewController (){
    UIImage *selectedImage;
}

@end

@implementation ProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view layoutIfNeeded];
    self.txtFirstName.userInteractionEnabled = NO;
    self.txtLastName.userInteractionEnabled = NO;
    self.email.userInteractionEnabled = NO;
    self.phone.userInteractionEnabled = NO;
    
    [self.popUpView setHidden:YES];
    
    
    [self.profileImage.layer setCornerRadius:self.profileImage.frame.size.height/2];
    self.profileImage.clipsToBounds=YES;
    
    [self.ChangeProfileBtn.layer setCornerRadius:self.ChangeProfileBtn.frame.size.height/2];
    self.ChangeProfileBtn.clipsToBounds=YES;
    
    [self.triangleSprite setHidden:YES];
    [self maskTriananlge:self.triangleSprite];
    
    
    
    //checking if agent or customer
    if ([[backendless.userService.currentUser getProperty:@"type"] isEqualToString: @"agent"])
    {
        [self.agentSpecificInfoView setHidden:NO];
    }
    else{
        [self.agentSpecificInfoView setHidden:YES];
    }
    
    
    if (![[backendless.userService.currentUser getProperty:@"profileImage"] isEqual:[NSNull null]]) {
        [self.profileImage sd_setImageWithURL:[NSURL URLWithString:[backendless.userService.currentUser getProperty:@"profileImage"]]
                             placeholderImage:[UIImage imageNamed:@"avatar.png"]
                                      options:SDWebImageRefreshCached];
        
    }
    else{
        [self.profileImage setImage:[UIImage imageNamed:@"avatar.png"]];
    }
    
    if (![backendless.userService.currentUser.name isEqual:[NSNull null]]) {
        if (![[backendless.userService.currentUser getProperty:@"QfistAndLastName"] isEqual:[NSNull null]]) {
            self.userName.text = [backendless.userService.currentUser getProperty:@"QfistAndLastName"];
            self.userNameSec.text = [backendless.userService.currentUser getProperty:@"QfistAndLastName"];
        }
        else{
            self.userName.text = [NSString stringWithFormat:@"%@ %@",[backendless.userService.currentUser getProperty:@"firstName"] ,[backendless.userService.currentUser getProperty:@"LastName"]];
            
            
            self.userNameSec.text = [NSString stringWithFormat:@"%@ %@",[backendless.userService.currentUser getProperty:@"firstName"] ,[backendless.userService.currentUser getProperty:@"LastName"]];;
        }
    }
    else{
        self.userName.text = @"";
        self.userNameSec.text =@"";
    }

   if (![[backendless.userService.currentUser getProperty:@"firstName"] isEqual:[NSNull null]]) {
    self.txtFirstName.text = [backendless.userService.currentUser getProperty:@"firstName"];
   }
   else if(![[backendless.userService.currentUser getProperty:@"QfistAndLastName"] isEqual:[NSNull null]]){
        
        NSArray *names = [[backendless.userService.currentUser getProperty:@"QfistAndLastName"] componentsSeparatedByString:@" "];
        
        if (names.count) {
            self.txtFirstName.text = names[0];
        }
    }
    else{
        self.txtFirstName.text = @"";
    }
    if (![[backendless.userService.currentUser getProperty:@"LastName"] isEqual:[NSNull null]]) {
        self.txtLastName.text = [backendless.userService.currentUser getProperty:@"LastName"];
    }
    else if(![[backendless.userService.currentUser getProperty:@"QfistAndLastName"] isEqual:[NSNull null]]){
        
        NSArray *names = [[backendless.userService.currentUser getProperty:@"QfistAndLastName"] componentsSeparatedByString:@" "];
        
        if (names.count > 1) {
            self.txtLastName.text = names[1];
        }
    }
    else{
        self.txtLastName.text = @"";
    }
    if (![[backendless.userService.currentUser getProperty:@"phone"] isEqual:[NSNull null]]) {
        self.phone.text = [backendless.userService.currentUser getProperty:@"phone"];
    }
    else if(![[backendless.userService.currentUser getProperty:@"bestPhoneNo"] isEqual:[NSNull null]]){
        self.phone.text = [backendless.userService.currentUser getProperty:@"bestPhoneNo"];
    }
    else{
        self.phone.text = @"";
    }
    
    //seting new fields
    if (![[backendless.userService.currentUser getProperty:@"companyName"] isEqual:[NSNull null]]) {
        
        self.txtCompany.text = [backendless.userService.currentUser getProperty:@"companyName"];
    }
    else{
        self.txtCompany.text = @"";
        }
    
    if (![[backendless.userService.currentUser getProperty:@"licenceNumber"] isEqual:[NSNull null]]) {
        
        self.txtLicenceNo.text = [backendless.userService.currentUser getProperty:@"licenceNumber"];

    }
    else{
        self.txtLicenceNo.text = @"";
    }
    //
    if (![[backendless.userService.currentUser getProperty:@"speciality"] isEqual:[NSNull null]]) {
        
        self.txtSpeciality.text = [backendless.userService.currentUser getProperty:@"speciality"];
        
    }
    else{
        self.txtSpeciality.text = @"";
    }

    //
    
    if (![[backendless.userService.currentUser getProperty:@"Lenguage"] isEqual:[NSNull null]]) {
        
        self.txtLenguage.text = [backendless.userService.currentUser getProperty:@"Lenguage"];
    }
    else{
        self.txtLenguage.text = @"";
    }

    self.email.text = [backendless.userService.currentUser getProperty:@"email"];
   
    
    
   
   
    
    
    // Do any additional setup after loading the view.
}
-(void)viewDidAppear:(BOOL)animated{
    [self.popUpView setHidden:YES];
    [self.triangleSprite setHidden:YES];
    
}
-(void)maskTriananlge:(UIView *)view {
    // Build a triangular path
    UIBezierPath *path = [UIBezierPath new];
    [path moveToPoint:(CGPoint){view.frame.size.width / 2, 0}];
    
    [path addLineToPoint:(CGPoint){0, view.frame.size.height}];
    
    
    [path addLineToPoint:(CGPoint){view.frame.size.width, view.frame.size.height}];
    [path addLineToPoint:(CGPoint){view.frame.size.width / 2, 0}];
    
    // Create a CAShapeLayer with this triangular path
    // Same size as the original imageView
    CAShapeLayer *mask = [CAShapeLayer new];
    mask.frame = view.bounds;
    mask.path = path.CGPath;
    
    // Mask the imageView's layer with this shape
    view.layer.mask = mask;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)editInformationAction:(UIButton *)sender {
    switch (sender.tag) {
        case 1:{
            self.txtFirstName.userInteractionEnabled = YES;
            [self.txtFirstName becomeFirstResponder];
            
            break;
        }
        case 2:{
            self.txtLastName.userInteractionEnabled = YES;
            [self.txtLastName becomeFirstResponder];
            break;
        }
        case 3:{
            self.email.userInteractionEnabled = YES;
            [self.email becomeFirstResponder];
            break;
        }
        case 4:{
            self.phone.userInteractionEnabled = YES;
            [self.phone becomeFirstResponder];
            break;
        }
        case 5:{
            self.txtCompany.userInteractionEnabled = YES;
            [self.txtCompany becomeFirstResponder];
            break;
        }

        case 6:{
            self.txtLicenceNo.userInteractionEnabled = YES;
            [self.txtLicenceNo becomeFirstResponder];
            break;
        }

        case 7:{
            self.txtSpeciality.userInteractionEnabled = YES;
            [self.txtSpeciality becomeFirstResponder];
            break;
        }

        case 8:{
            self.txtLenguage.userInteractionEnabled = YES;
            [self.txtLenguage becomeFirstResponder];
            break;
        }

        default:
            break;
    }
}
- (IBAction)backAction:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)menueAction:(UIButton *)sender {
    if ([self.popUpView isHidden]) {
        [self.popUpView setHidden:NO];
        [self.triangleSprite setHidden:NO];
        
    }
    else
    {
        [self.popUpView setHidden:YES];
        [self.triangleSprite setHidden:YES];
        
    }
    
}

#pragma mark Image Picker And Delegates

- (void)showImagePicker:(UIImagePickerControllerSourceType)sourceType sender:(id)sender {
    
    UIImagePickerController *controler = [[UIImagePickerController alloc] init];
    if (![UIImagePickerController isSourceTypeAvailable:sourceType]) {
        /*AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Permission Error" andText:@"Permission not granted"  andCancelButton:NO forAlertType:AlertFailure];
         
         [alert show];*/
        
        //loder remove
        return;
    }
    controler.sourceType = sourceType;
    controler.delegate = self;
    if ([UIImagePickerController isSourceTypeAvailable:sourceType]) {
        
        [self presentViewController:controler animated:YES completion:nil];
        
    }
}
-(UIImage *)getScaledToRequiredSizedImage :(int)requiredSize :(UIImage *)sentImage{
    int scale = 1;
    
    while (sentImage.size.width / scale / 2 >= requiredSize
           && sentImage.size.height / scale / 2 >= requiredSize)
        scale *= 2;
    
    return [self imageWithImage:selectedImage scaledToSize:CGSizeMake(selectedImage.size.width/scale, selectedImage.size.height/scale)];
}
- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    //UIGraphicsBeginImageContext(newSize);
    // In next line, pass 0.0 to use the current device's pixel scaling factor (and thus account for Retina resolution).
    // Pass 1.0 to force exact pixel size.
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    NSLog(@"Picker returned successfully.");
    
    NSURL *mediaUrl;
    mediaUrl = (NSURL *)[info valueForKey:UIImagePickerControllerMediaURL];
    if (mediaUrl == nil) {
        selectedImage = (UIImage *) [info valueForKey:UIImagePickerControllerEditedImage];
        if (selectedImage == nil) {
            selectedImage= (UIImage *) [info valueForKey:UIImagePickerControllerOriginalImage];
            NSLog(@"Original image picked.");
            selectedImage = [self scaleAndRotateImage:selectedImage];
            [self.profileImage setImage:selectedImage];
        }
        else {
            NSLog(@"Edited image picked.");
        }
    }
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    NSLog(@"Image url is : %@",[mediaUrl absoluteString]);
}

- (UIImage *)scaleAndRotateImage:(UIImage *) image {
    
    CGImageRef imgRef = image.CGImage;
    
    CGFloat width = CGImageGetWidth(imgRef);
    CGFloat height = CGImageGetHeight(imgRef);
    
    
    CGAffineTransform transform = CGAffineTransformIdentity;
    CGRect bounds = CGRectMake(0, 0, width, height);
    
    
    CGFloat scaleRatio = bounds.size.width / width;
    CGSize imageSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef));
    CGFloat boundHeight;
    UIImageOrientation orient = image.imageOrientation;
    switch(orient) {
            
        case UIImageOrientationUp: //EXIF = 1
            transform = CGAffineTransformIdentity;
            break;
            
        case UIImageOrientationUpMirrored: //EXIF = 2
            transform = CGAffineTransformMakeTranslation(imageSize.width, 0.0);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            break;
            
        case UIImageOrientationDown: //EXIF = 3
            transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationDownMirrored: //EXIF = 4
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
            transform = CGAffineTransformScale(transform, 1.0, -1.0);
            break;
            
        case UIImageOrientationLeftMirrored: //EXIF = 5
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationLeft: //EXIF = 6
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationRightMirrored: //EXIF = 7
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeScale(-1.0, 1.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        case UIImageOrientationRight: //EXIF = 8
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        default:
            [NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];
            
    }
    
    UIGraphicsBeginImageContext(bounds.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if (orient == UIImageOrientationRight || orient == UIImageOrientationLeft) {
        CGContextScaleCTM(context, -scaleRatio, scaleRatio);
        CGContextTranslateCTM(context, -height, 0);
    }
    else {
        CGContextScaleCTM(context, scaleRatio, -scaleRatio);
        CGContextTranslateCTM(context, 0, -height);
    }
    
    CGContextConcatCTM(context, transform);
    
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imgRef);
    UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return imageCopy;
}
- (IBAction)changeProfileImageAction:(UIButton *)sender {
    NSArray *colors = [NSArray arrayWithObjects:@"Gallery" , @"Camera", nil];
    
    [ActionSheetStringPicker showPickerWithTitle:@"Select Image Source."
                                            rows:colors
                                initialSelection:0
                                       doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
                                           
                                           if (selectedIndex == 1) {
                                               [self showImagePicker:UIImagePickerControllerSourceTypeCamera sender:nil];
                                               
                                           }
                                           else{
                                               [self showImagePicker:UIImagePickerControllerSourceTypePhotoLibrary sender:nil];
                                               
                                           }
                                           
                                       }
                                     cancelBlock:^(ActionSheetStringPicker *picker) {
                                         NSLog(@"Block Picker Canceled");
                                     }
                                          origin:sender];
    
    
}
    -(void)uploadAsyncWithImageName :(NSString *)imageName andNewRegistereddUser :(BackendlessUser *)newRegisteredUser {
        
        NSLog(@"\n============ Uploading file  with the ASYNC API ============");
        
        
        
        int REQUIRED_SIZE = 100;
        selectedImage = [self getScaledToRequiredSizedImage:REQUIRED_SIZE :selectedImage];
        NSString *num =   [NSString stringWithFormat:@"%u", arc4random() % 100000000];
        NSData *imageData = UIImageJPEGRepresentation(selectedImage, 1.0);
        
        [backendless.fileService upload:[NSString stringWithFormat:@"myfiles/%@%@.jpg", imageName,num]  content:imageData
                               response:^(BackendlessFile *uploadedFile) {
                                   NSLog(@"File has been uploaded. File URL is - %@", uploadedFile.fileURL);
                                   
                                   [self updateImageUrl:newRegisteredUser :uploadedFile.fileURL];
                                   
                               }
                                  error:^(Fault *fault) {
                                      
                                      
                                      if ([fault.faultCode isEqualToString:@"-1009"]){
                                          [SVProgressHUD dismiss];
                                          AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Login Failed!!!" andText:@"The Internet connection appears to be offline." andCancelButton:NO forAlertType:AlertFailure];
                                          [alert show];
                                          return;
                                      }
                                      
                                      NSLog(@"Server reported an error: %@", fault);
                                      
                                      NSString *errorMsg = [NSString stringWithFormat:@"Server reported an error. Details = %@", fault.description];
                                      NSLog(@"FAULT = %@ <%@>", fault.message, fault.detail);
                                      [SqbLogger logIt:[NSString stringWithFormat:@"%@ - %@" , fault.faultCode , fault.message ]] ;
                                      
                                      AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Oops!!!" andText:@"Something Unexpected has happened. Please Send Report to Developers." andCancelButton:YES forAlertType:AlertInfo andColor:[UIColor purpleColor] withCompletionHandler:^(AMSmoothAlertView *alertView, UIButton *btton) {
                                          if(btton == alertView.defaultButton) {
                                              NSLog(@"Default");
                                              [SqbLogger submitCrashReport:[NSString stringWithFormat:@"%@ - %@" , fault.faultCode , fault.message ]];
                                              
                                              [alert dismissAlertView];
                                              
                                          }
                                          else {
                                              NSLog(@"Others");
                                              [alert dismissAlertView];
                                              
                                          }
                                      }];
                                      [alert.defaultButton setTitle:@"Send Report" forState:UIControlStateNormal];
                                      [alert.defaultButton.titleLabel setFont: [alert.defaultButton.titleLabel.font fontWithSize: 12]];
                                      
                                      [alert.cancelButton setTitle:@"Close" forState:UIControlStateNormal];
                                      [alert.cancelButton.titleLabel setFont: [alert.defaultButton.titleLabel.font fontWithSize: 12]];
                                      
                                      
                                      [alert show];

                                      
                                      [SVProgressHUD dismiss];
                                      
                                  }];
    }
-(void)updateImageUrl:(BackendlessUser *)imageUpdateUser :(NSString *)newUrl
    {
        
        [imageUpdateUser setProperty:@"profileImage" object:[NSString stringWithFormat:@"%@", newUrl]];
        
        id<IDataStore> dataStore = [backendless.persistenceService of:[BackendlessUser class]];
        [dataStore save:imageUpdateUser response:^(id result) {
            [SVProgressHUD dismiss];
                
        
        } error:^(Fault *error) {
            
            
            if ([error.faultCode isEqualToString:@"-1009"]){
                [SVProgressHUD dismiss];
                AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Login Failed!!!" andText:@"The Internet connection appears to be offline." andCancelButton:NO forAlertType:AlertFailure];
                [alert show];
                return;
            }
            
            
            NSLog(@"Error At imageUrl Update %@", error.description);
            NSString *errorMsg = [NSString stringWithFormat:@"Error At imageUrl Update. Details = %@", error.description];
            NSLog(@"FAULT = %@ <%@>", error.message, error.detail);
            [SqbLogger logIt:[NSString stringWithFormat:@"%@ - %@" , error.faultCode , error.message ]] ;
            
            AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Oops!!!" andText:@"Something Unexpected has happened. Please Send Report to Developers." andCancelButton:YES forAlertType:AlertInfo andColor:[UIColor purpleColor] withCompletionHandler:^(AMSmoothAlertView *alertView, UIButton *btton) {
                if(btton == alertView.defaultButton) {
                    NSLog(@"Default");
                    [SqbLogger submitCrashReport:[NSString stringWithFormat:@"%@ - %@" , error.faultCode , error.message ]];
                    
                    [alert dismissAlertView];
                    
                }
                else {
                    NSLog(@"Others");
                    [alert dismissAlertView];
                    
                }
            }];
            [alert.defaultButton setTitle:@"Send Report" forState:UIControlStateNormal];
            [alert.defaultButton.titleLabel setFont: [alert.defaultButton.titleLabel.font fontWithSize: 12]];
            
            [alert.cancelButton setTitle:@"Close" forState:UIControlStateNormal];
            [alert.cancelButton.titleLabel setFont: [alert.defaultButton.titleLabel.font fontWithSize: 12]];
            
            
            [alert show];

            
            
            [SVProgressHUD dismiss];
            
        }];
    }

- (IBAction)savaChangesAction:(UIButton *)sender {
    [SVProgressHUD showWithStatus:@"updating User..."];
    
    
    BackendlessUser *updateUser = backendless.userService.currentUser ;
    id<IDataStore> dataStore = [backendless.persistenceService of:[BackendlessUser class]];
    [updateUser setProperty:@"firstName" object:self.txtFirstName.text];
    [updateUser setProperty:@"LastName" object:self.txtLastName.text];
    [updateUser setProperty:@"email" object:self.email.text];
    [updateUser setProperty:@"phone" object:self.phone.text];
    //[updateUser setProperty:@"name" object:[NSString stringWithFormat:@"%@ %@",self.txtFirstName.text,self.txtLastName.text]];
    //new fields
     [updateUser setProperty:@"companyName" object:self.txtCompany.text];
     [updateUser setProperty:@"licenceNumber" object:self.txtLicenceNo.text];
     [updateUser setProperty:@"speciality" object:self.txtSpeciality.text];
     [updateUser setProperty:@"Lenguage" object:self.txtLenguage.text];
   
    
    [dataStore save:updateUser response:^(BackendlessUser *updatedUser)
    {
        if(selectedImage){
          [self uploadAsyncWithImageName:updatedUser.objectId andNewRegistereddUser:updatedUser];
        }
        [SVProgressHUD dismiss];
    }
    error:^(Fault *error) {
        [SVProgressHUD dismiss];
        
        NSString *errorMsg = [NSString stringWithFormat:@"Failed to Update. Details = %@", error.description];
        NSLog(@"FAULT = %@ <%@>", error.message, error.detail);
        [SqbLogger logIt:[NSString stringWithFormat:@"%@ - %@" , error.faultCode , error.message ]] ;
        
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Oops!!!" andText:@"Something Unexpected has happened. Please Send Report to Developers." andCancelButton:YES forAlertType:AlertInfo andColor:[UIColor purpleColor] withCompletionHandler:^(AMSmoothAlertView *alertView, UIButton *btton) {
            if(btton == alertView.defaultButton) {
                NSLog(@"Default");
                [SqbLogger submitCrashReport:[NSString stringWithFormat:@"%@ - %@" , error.faultCode , error.message ]];
                
                [alert dismissAlertView];
                
            }
            else {
                NSLog(@"Others");
                [alert dismissAlertView];
                
            }
        }];
        [alert.defaultButton setTitle:@"Send Report" forState:UIControlStateNormal];
        [alert.defaultButton.titleLabel setFont: [alert.defaultButton.titleLabel.font fontWithSize: 12]];
        
        [alert.cancelButton setTitle:@"Close" forState:UIControlStateNormal];
        [alert.cancelButton.titleLabel setFont: [alert.defaultButton.titleLabel.font fontWithSize: 12]];
        
        
        [alert show];

        
        
    }];
    
}
@end
