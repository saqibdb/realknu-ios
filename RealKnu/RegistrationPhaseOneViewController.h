//
//  RegistrationPhaseOneViewController.h
//  RealKnu
//
//  Created by Shirley-Mac3 on 2/2/17.
//  Copyright © 2017 Jason Macbook. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegistrationPhaseOneViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *txtFirstName;
@property (weak, nonatomic) IBOutlet UITextField *txtLastName;
- (IBAction)nextAction:(UIButton *)sender;
- (IBAction)backAction:(UIButton *)sender;
- (IBAction)registerWithFacebookAction:(UIButton *)sender;


@end
