//
//  RegistrationPhaseOneViewController.m
//  RealKnu
//
//  Created by Shirley-Mac3 on 2/2/17.
//  Copyright © 2017 Jason Macbook. All rights reserved.
//
#import "RegistrationPhaseTwoViewController.h"
#import "RegistrationPhaseOneViewController.h"
#import "AMSmoothAlertView.h"
#import "SVProgressHUD.h"
#import "SqbLogger.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "Backendless.h"
#import <INTULocationManager/INTULocationManager.h>

@interface RegistrationPhaseOneViewController ()

@end

@implementation RegistrationPhaseOneViewController

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [[self view] endEditing:YES];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.txtFirstName.text = [SqbLogger getCustomerFirstName];
    self.txtLastName.text = [SqbLogger getCustomerLastName];
    
    
    NSLog(@"%@" , [SqbLogger getCustomerFirstName]) ;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"PhaseOneToPhaseTwo"]) {
        RegistrationPhaseTwoViewController *vc = segue.destinationViewController ;
        vc.firstName = self.txtFirstName.text;
        vc.lastName = self.txtLastName.text;
    }
}

- (IBAction)nextAction:(UIButton *)sender {
    
    if (!self.txtFirstName.text.length) {
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"" andText:@"Firs Name cannot be Empty" andCancelButton:NO forAlertType:AlertInfo];
        [alert show];
    }
    else if (!self.txtLastName.text.length)
    {
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"" andText:@"Last Name cannot be Empty" andCancelButton:NO forAlertType:AlertInfo];
        [alert show];
    }
    else
    {
        
        [SqbLogger setLastCustomerFirstName:self.txtFirstName.text andLastName:self.txtLastName.text] ;
        [self performSegueWithIdentifier:@"PhaseOneToPhaseTwo" sender:self];
    }
}

- (IBAction)backAction:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];

}

- (IBAction)registerWithFacebookAction:(UIButton *)sender {
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logInWithReadPermissions:@[@"public_profile",@"email"] fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        if (result.isCancelled) {
            NSLog(@"Token: %@", result.token);
        } else {
            if ([FBSDKAccessToken currentAccessToken]) {
                FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc]initWithGraphPath:@"me" parameters:@{@"fields" : @"id,name,email,first_name,last_name,picture"                                                                                                            }];
                [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                    if (result) {
                        NSLog(@"Got in......");
                        NSString *email = [NSString stringWithFormat:@"%@@Shirley.com" , [result objectForKey:@"id"]];
                        if ([result objectForKey:@"email"]) {
                            email = [result objectForKey:@"email"];
                        }
                        [SVProgressHUD showWithStatus:@"Loading..."];
                        [self checkIfUserAlreadyExist:email andFacebookResult:result];

                    }
                }];
            }
        }
    }];
}


-(void)checkIfUserAlreadyExist:(NSString *)email andFacebookResult :(id)result
{
    
    BackendlessDataQuery *query = [BackendlessDataQuery query];
    query.whereClause = [NSString stringWithFormat:@"email = '%@' AND type = 'customer'",email];
    id<IDataStore> dataStore = [backendless.persistenceService of:[BackendlessUser class]];
    [dataStore find:query response:^(BackendlessCollection *userData) {
        
        if(userData.data.count>0)
        {
            BackendlessUser *newUser = [BackendlessUser new];
            for(BackendlessUser *user in userData.data)
            {
                newUser.email = user.name;
                newUser.password = @"123";
                [newUser setName:user.name];
                [self loginWithUser:newUser];
                break;
            }
            
        }
        else{
            [self getCurrentLocationandFacebookResult:result];
        }
        
    } error:^(Fault *error) {
        [SVProgressHUD dismiss];
        NSLog(@"Error Found at photo  %@",error.detail);
    }];
}

-(void)loginWithUser :(BackendlessUser *)loginUser
{
    [SVProgressHUD show];
    [backendless.userService login:loginUser.name password:loginUser.password response:^(BackendlessUser *userLogged) {
        [SVProgressHUD dismiss];
        [backendless.userService setStayLoggedIn:YES];
        

        
        [self performSegueWithIdentifier:@"phaseOneToMap" sender:self];
    }
                             error:^(Fault *error) {
                                 [SVProgressHUD dismiss];
                                 AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Opps!!!" andText:@"Something Unexpected has happened. Please try again later." andCancelButton:NO forAlertType:AlertFailure];
                                 [alert show];
                                 
                             }];
}

-(void)getCurrentLocationandFacebookResult :(id)result{
    
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    [locMgr requestLocationWithDesiredAccuracy:INTULocationAccuracyCity
                                       timeout:10.0
                          delayUntilAuthorized:YES  // This parameter is optional, defaults to NO if omitted
                                         block:^(CLLocation *currentLocation, INTULocationAccuracy achievedAccuracy, INTULocationStatus status) {
                                             if (status == INTULocationStatusSuccess) {
                                                 [self registerCustomerWithLocation:currentLocation andFacebookResult:result];
                                             }
                                             else if (status == INTULocationStatusTimedOut) {
                                                 [SVProgressHUD dismiss];
                                                 
                                                 AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"" andText:@"TimeOut" andCancelButton:NO forAlertType:AlertFailure];
                                                 
                                                 [alert show];
                                             }
                                             else {
                                                 [SVProgressHUD dismiss];
                                                 
                                                 AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error!" andText:@"Error in finding your location." andCancelButton:NO forAlertType:AlertFailure];
                                                 
                                                 [alert show];
                                             }
                                         }];
    
}

-(void)registerCustomerWithLocation:(CLLocation *)loc andFacebookResult :(id)result{
    [SVProgressHUD show];
    BackendlessUser *user = [BackendlessUser new];
    NSString *email = [NSString stringWithFormat:@"%@@Shirley.com" , [result objectForKey:@"id"]];
    if ([result objectForKey:@"email"]) {
        email = [result objectForKey:@"email"];
    }
    
    
    [user setProperty:@"email" object:email];
    [user setProperty:@"firstName" object:[result objectForKey:@"first_name"]];
    [user setProperty:@"LastName" object:[result objectForKey:@"last_name"]];
    [user setProperty:@"name" object:[result objectForKey:@"id"]];
    [user setProperty:@"password" object:@"123"];
    [user setProperty:@"phone" object:@"0000000000000"];
    [user setProperty:@"type" object:@"customer"];
    [user setProperty:@"longitude" object:[NSString stringWithFormat:@"%f",loc.coordinate.longitude]];
    [user setProperty:@"latitude" object:[NSString stringWithFormat:@"%f",loc.coordinate.latitude]];
    
    
    //update usersData
    
    [backendless.userService registering:user response:^(BackendlessUser *userRegistered) {
        //[SVProgressHUD dismiss];
        userRegistered.email = userRegistered.name;
        userRegistered.password = @"123";
        [self loginWithUser:userRegistered];
        
        
    }
                                   error:^(Fault *error) {
                                       if ([error.faultCode isEqualToString:@"-1009"]){
                                           [SVProgressHUD dismiss];
                                           AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Login Failed!!!" andText:@"The Internet connection appears to be offline." andCancelButton:NO forAlertType:AlertFailure];
                                           [alert show];
                                           return;
                                       }
                                       
                                       [SqbLogger logIt:[NSString stringWithFormat:@"%@ - %@" , error.faultCode , error.message ]] ;
                                       
                                       AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Oops!!!" andText:@"Something Unexpected has happened. Please Send Report to Developers." andCancelButton:YES forAlertType:AlertInfo andColor:[UIColor purpleColor] withCompletionHandler:^(AMSmoothAlertView *alertView, UIButton *btton) {
                                           if(btton == alertView.defaultButton) {
                                               NSLog(@"Default");
                                               [SqbLogger submitCrashReport:[NSString stringWithFormat:@"%@ - %@" , error.faultCode , error.message ]];
                                               
                                               [alert dismissAlertView];
                                               
                                           }
                                           else {
                                               NSLog(@"Others");
                                               [alert dismissAlertView];
                                               
                                           }
                                       }];
                                       [alert.defaultButton setTitle:@"Send Report" forState:UIControlStateNormal];
                                       [alert.defaultButton.titleLabel setFont: [alert.defaultButton.titleLabel.font fontWithSize: 12]];
                                       
                                       [alert.cancelButton setTitle:@"Close" forState:UIControlStateNormal];
                                       [alert.cancelButton.titleLabel setFont: [alert.defaultButton.titleLabel.font fontWithSize: 12]];
                                       
                                       
                                       [alert show];
                                       
                                       [SVProgressHUD dismiss];
                                       
                                   }];
    
}

@end
