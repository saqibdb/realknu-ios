//
//  RegistrationPhaseTwoViewController.h
//  RealKnu
//
//  Created by Shirley-Mac3 on 2/2/17.
//  Copyright © 2017 Jason Macbook. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegistrationPhaseTwoViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtPhone;
- (IBAction)nextAction:(UIButton *)sender;
@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *lastName;
- (IBAction)backAction:(UIButton *)sender;



@end
