//
//  RegistrationPhaseTwoViewController.m
//  RealKnu
//
//  Created by Shirley-Mac3 on 2/2/17.
//  Copyright © 2017 Jason Macbook. All rights reserved.
//

#import "RegistrationPhaseTwoViewController.h"
#import "SVProgressHUD.h"
#import "Backendless.h"
#import "AMSmoothAlertView.h"
#import "SVProgressHUD.h"
#import <INTULocationManager/INTULocationManager.h>
#import "SqbLogger.h"

@interface RegistrationPhaseTwoViewController ()

@end

@implementation RegistrationPhaseTwoViewController

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [[self view] endEditing:YES];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    if ([[SqbLogger getCustomerFirstName] isEqualToString:self.firstName] && [[SqbLogger getCustomerLastName] isEqualToString:self.lastName]) {
        self.txtEmail.text = [SqbLogger getCustomerEmail];
        self.txtPhone.text = [SqbLogger getCustomerPhone];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - methods
-(void)getCurrentLocation{
    
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    [locMgr requestLocationWithDesiredAccuracy:INTULocationAccuracyCity
                                       timeout:10.0
                          delayUntilAuthorized:YES  // This parameter is optional, defaults to NO if omitted
                                         block:^(CLLocation *currentLocation, INTULocationAccuracy achievedAccuracy, INTULocationStatus status) {
                                             if (status == INTULocationStatusSuccess) {
                                                 [self registerCustomerWithLocation:currentLocation];
                                             }
                                             else if (status == INTULocationStatusTimedOut) {
                                                 [SVProgressHUD dismiss];
                                                 
                                                 AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"" andText:@"TimeOut" andCancelButton:NO forAlertType:AlertFailure];
                                                 
                                                 [alert show];
                                             }
                                             else {
                                                 [SVProgressHUD dismiss];
                                                 
                                                 AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error!" andText:@"Error in finding your location." andCancelButton:NO forAlertType:AlertFailure];
                                                 
                                                 [alert show];
                                             }
                                         }];
    
}
-(void)registerCustomerWithLocation:(CLLocation *)loc{
    [SVProgressHUD show];
    BackendlessUser *user = [BackendlessUser new];
    [user setProperty:@"email" object:self.txtEmail.text];
    [user setProperty:@"firstName" object:self.firstName];
    [user setProperty:@"LastName" object:self.lastName];
    [user setProperty:@"name" object:self.firstName];
    [user setProperty:@"password" object:@"1234567"];
    [user setProperty:@"phone" object:self.txtPhone.text];
    [user setProperty:@"type" object:@"customer"];
    [user setProperty:@"longitude" object:[NSString stringWithFormat:@"%f",loc.coordinate.longitude]];
    [user setProperty:@"latitude" object:[NSString stringWithFormat:@"%f",loc.coordinate.latitude]];
    [user setProperty:@"loginStatus" object:@"loggedIn"];

    
    
   
    
    //update usersData
    
    [backendless.userService registering:user response:^(BackendlessUser *userRegistered) {
        //[SVProgressHUD dismiss];
        userRegistered.email = userRegistered.name;
        userRegistered.password = @"123";
        [self loginWithUser:userRegistered];
        
        
    } error:^(Fault *error) {
        if ([error.faultCode isEqualToString:@"-1009"]){
            [SVProgressHUD dismiss];
            AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Login Failed!!!" andText:@"The Internet connection appears to be offline." andCancelButton:NO forAlertType:AlertFailure];
            [alert show];
            return;
        }
        
        
        if ([error.faultCode isEqualToString:@"3033"]){
            [SVProgressHUD dismiss];
            AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Login Failed!!!" andText:@"This UserName is already Registered as an Agent, Please Register with another User Name on previous screen." andCancelButton:NO forAlertType:AlertFailure];
            [alert show];
            return;
        }
        
        
        [SqbLogger logIt:[NSString stringWithFormat:@"%@ - %@" , error.faultCode , error.message ]] ;
        
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Oops!!!" andText:@"Something Unexpected has happened. Please Send Report to Developers." andCancelButton:YES forAlertType:AlertInfo andColor:[UIColor purpleColor] withCompletionHandler:^(AMSmoothAlertView *alertView, UIButton *btton) {
            if(btton == alertView.defaultButton) {
                NSLog(@"Default");
                [SqbLogger submitCrashReport:[NSString stringWithFormat:@"%@ - %@" , error.faultCode , error.message ]];
                
                [alert dismissAlertView];
                
            }
            else {
                NSLog(@"Others");
                [alert dismissAlertView];
                
            }
        }];
        [alert.defaultButton setTitle:@"Send Report" forState:UIControlStateNormal];
        [alert.defaultButton.titleLabel setFont: [alert.defaultButton.titleLabel.font fontWithSize: 12]];
        
        [alert.cancelButton setTitle:@"Close" forState:UIControlStateNormal];
        [alert.cancelButton.titleLabel setFont: [alert.defaultButton.titleLabel.font fontWithSize: 12]];
        
        
        [alert show];
        
        [SVProgressHUD dismiss];
        
    }];
    
}
-(void)checkIfUserAlreadyExist:(NSString *)email{
    [SVProgressHUD showWithStatus:@"Loading..."];
    BackendlessDataQuery *query = [BackendlessDataQuery query];
    query.whereClause = [NSString stringWithFormat:@"email = '%@' AND type = 'customer'",self.txtEmail.text];
    id<IDataStore> dataStore = [backendless.persistenceService of:[BackendlessUser class]];
    [dataStore find:query response:^(BackendlessCollection *userData) {
        
        if(userData.data.count>0)
        {
            BackendlessUser *newUser = [BackendlessUser new];
            for(BackendlessUser *user in userData.data)
            {
                newUser.email = user.name;
                newUser.password = @"1234567";
                [newUser setName:user.name];
                [self loginWithUser:newUser];
                break;
            }
            
        }
        else{
            [self getCurrentLocation];
        }
        
    } error:^(Fault *error) {
        [SVProgressHUD dismiss];
        NSLog(@"Error Found at photo  %@",error.detail);
        if ([error.faultCode isEqualToString:@"-1009"]){
            [SVProgressHUD dismiss];
            AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Login Failed!!!" andText:@"The Internet connection appears to be offline." andCancelButton:NO forAlertType:AlertFailure];
            [alert show];
            return;
        }
    }];
}
-(void)loginWithUser :(BackendlessUser *)loginUser
{
    [SVProgressHUD show];
    [backendless.userService login:loginUser.name password:loginUser.password response:^(BackendlessUser *userLogged) {
        [SVProgressHUD dismiss];
        [backendless.userService setStayLoggedIn:YES];
        
        [SqbLogger setLastCustomerEmail:self.txtEmail.text andPhone:self.txtPhone.text];
        
        [self performSegueWithIdentifier:@"phaseTwoToMap" sender:self];
    }
                             error:^(Fault *error) {
                                 [SVProgressHUD dismiss];
                                 AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Opps!!!" andText:@"Something Unexpected has happened. Please try again later." andCancelButton:NO forAlertType:AlertFailure];
                                 [alert show];
                                 
                             }];
}

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
}
#pragma mark - Actions
- (IBAction)nextAction:(UIButton *)sender {
    if (!self.txtEmail.text.length) {
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"" andText:@"Email cannot be Empty" andCancelButton:NO forAlertType:AlertInfo];
        [alert show];
    }
    else
    {
        if (![self NSStringIsValidEmail:self.txtEmail.text]) {
            AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Warning" andText:@"Email is not valid." andCancelButton:NO forAlertType:AlertInfo];
            [alert show];
        }
        else{
            [self checkIfUserAlreadyExist:self.txtEmail.text];
        }
    }
}


-(BOOL) NSStringIsValidEmail:(NSString *)checkString{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}



- (IBAction)backAction:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];

}
@end
