//
//  SignUpLoginTableViewCell.h
//  RealKnu
//
//  Created by Shirley on 2/23/17.
//  Copyright © 2017 Jason Macbook. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CCMBorderView/CCMBorderView.h>

@interface SignUpLoginTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *loginBtn;
@property (weak, nonatomic) IBOutlet UIButton *signUpBtn;
@property (weak, nonatomic) IBOutlet UIView *loginLine;

@property (weak, nonatomic) IBOutlet UIView *signUpLine;

@property (weak, nonatomic) IBOutlet CCMBorderView *loginView;
@property (weak, nonatomic) IBOutlet UIView *signUpView;

@property (weak, nonatomic) IBOutlet UITextField *sUserName;
@property (weak, nonatomic) IBOutlet UITextField *sPassword;
@property (weak, nonatomic) IBOutlet UITextField *sEmail;
@property (weak, nonatomic) IBOutlet UIButton *sAlreadyMemberBtn;
@property (weak, nonatomic) IBOutlet UIButton *sAcceptbtn;
@property (weak, nonatomic) IBOutlet UIButton *sAcceptLogoBtn;

@property (weak, nonatomic) IBOutlet UITextField *lUserName;
@property (weak, nonatomic) IBOutlet UITextField *lPassword;
@property (weak, nonatomic) IBOutlet UIImageView *sCheckIcon;

@property (weak, nonatomic) IBOutlet UIButton *sSignUpFacebookButton;


- (IBAction)loginBtnAction:(id)sender;
- (IBAction)signUpBtnAction:(id)sender;
- (IBAction)sAlreadyMemberAction:(id)sender;
- (IBAction)sAcceptAction:(id)sender;
- (IBAction)sAcceptLogoBtn:(id)sender;
- (IBAction)lLoginAction:(id)sender;
- (IBAction)sSignUpAction:(id)sender;
- (IBAction)sSignUpFacebookAction:(UIButton *)sender;
- (IBAction)lLoginWithFacebookAction:(UIButton *)sender;

@end
