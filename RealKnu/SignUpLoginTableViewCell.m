//
//  SignUpLoginTableViewCell.m
//  RealKnu
//
//  Created by Shirley on 2/23/17.
//  Copyright © 2017 Jason Macbook. All rights reserved.
//

#import "SignUpLoginTableViewCell.h"
#import "Backendless.h"
#import "AMSmoothAlertView.h"
#import "SVProgressHUD.h"
#import "TermsAndConditionsPopUp.h"
#import "UIViewController+CWPopup.h"
#import <INTULocationManager/INTULocationManager.h>
#import "SqbLogger.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>




@implementation SignUpLoginTableViewCell{
    BOOL isEmailGotten;
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.loginLine.hidden = NO;
    self.loginView.hidden = NO;
    self.signUpLine.hidden = YES;
    self.signUpView.hidden = YES;
    
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    [self addGestureRecognizer:singleFingerTap];
    
}



- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer {
    
    [self.sUserName resignFirstResponder];
    [self.sPassword resignFirstResponder];
    [self.sEmail resignFirstResponder];
    [self.lUserName resignFirstResponder];
    [self.lPassword resignFirstResponder];
    
    
    //Do stuff here...
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)loginBtnAction:(id)sender {
    
    self.loginLine.hidden = NO;
    self.loginView.hidden = NO;
    self.signUpLine.hidden = YES;
    self.signUpView.hidden = YES;
    
}

- (IBAction)signUpBtnAction:(id)sender {
    self.loginLine.hidden = YES;
    self.loginView.hidden = YES;
    self.signUpLine.hidden = NO;
    self.signUpView.hidden = NO;
}

- (IBAction)sAlreadyMemberAction:(id)sender {
    self.loginLine.hidden = NO;
    self.loginView.hidden = NO;
    self.signUpLine.hidden = YES;
    self.signUpView.hidden = YES;
}

- (IBAction)sAcceptAction:(id)sender {
    
    TermsAndConditionsPopUp *popUp = [[TermsAndConditionsPopUp alloc] initWithNibName:@"TermsAndConditionsPopUp" bundle:nil];
    popUp.view.superview.layer.cornerRadius = 0;
    [[self getViewController] presentPopupViewController:popUp animated:YES completion:^(void) {
        
        [popUp.closeBtn addTarget:self action:@selector(dismissPopup) forControlEvents:UIControlEventTouchUpInside];
        [popUp.acceptBtn addTarget:self action:@selector(AcceptAction) forControlEvents:UIControlEventTouchUpInside];
        [popUp.closeBtn.layer setCornerRadius:popUp.closeBtn.frame.size.height/2];
        popUp.closeBtn.clipsToBounds=YES;
    }];
    
    
}
-(void)AcceptAction{
    self.sCheckIcon.tag = 0;
    [self.sCheckIcon setImage:[UIImage imageNamed:@"checkIcon"]];
    
    [self dismissPopup];
}
-(void)dismissPopup{
    
    [[self getViewController] dismissPopupViewControllerAnimated:YES completion:nil];
}

- (UIViewController *)getViewController
{
    id vc = [self nextResponder];
    while(![vc isKindOfClass:[UIViewController class]] && vc!=nil)
    {
        vc = [vc nextResponder];
    }
    
    return vc;
}

- (IBAction)sAcceptLogoBtn:(id)sender {
    if (self.sCheckIcon.tag) {
        self.sCheckIcon.tag = 0;
        [self.sCheckIcon setImage:[UIImage imageNamed:@"checkIcon"]];
    }
    else{
        self.sCheckIcon.tag = 1;
        [self.sCheckIcon setImage:[UIImage imageNamed:@"uncheckIcon"]];
    }
}

- (IBAction)lLoginAction:(id)sender {
    BackendlessUser *user = [BackendlessUser new];
    if (self.lUserName.text.length == 0 || self.lPassword.text.length == 0) {
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Sorry" andText:@"Fields cannot be empty" andCancelButton:NO forAlertType:AlertInfo];
        [alert show];
        return;
    }
    [self.lUserName resignFirstResponder];
    [self.lPassword resignFirstResponder];
    [user setProperty:@"name" object:self.lUserName.text];
    user.password = self.lPassword.text;
    //user.email = self.lUserName.text;
    [self loginWithUser:user];
}

- (IBAction)sSignUpAction:(id)sender {

    if (!self.sUserName.text.length || !self.sEmail.text.length || !self.sPassword.text.length) {
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Sorry" andText:@"Fields cannot be empty" andCancelButton:NO forAlertType:AlertInfo];
        [alert show];
    }
    else if (self.sCheckIcon.tag == 1){
        
        [self.sCheckIcon setImage:[UIImage imageNamed:@"uncheckIcon"]];
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"" andText:@"Please Accept Terms & Conditions" andCancelButton:NO forAlertType:AlertInfo];
        [alert show];
        
    }
    else{
        
        if (![self NSStringIsValidEmail:self.sEmail.text]) {
            AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Warning" andText:@"Email is not valid." andCancelButton:NO forAlertType:AlertInfo];
            [alert show];
            return;
        }
        
        
        
        
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        [defaults setObject:self.sUserName.text forKey:@"username"];
        [defaults setObject:self.sPassword.text forKey:@"password"];
        [defaults setObject:self.sEmail.text forKey:@"email"];
        [defaults setBool:YES forKey:@"isEmailGotten"];
        [defaults synchronize];
        [[self getViewController] performSegueWithIdentifier:@"SignUpToAnswerQuestions" sender:self] ;
    }
}







-(BOOL) NSStringIsValidEmail:(NSString *)checkString{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}





#pragma mark - Methods
-(void)loginWithUser :(BackendlessUser *)loginUser
{
    [SVProgressHUD showWithStatus:@"Checking Credentials..."];
    [backendless.userService login:[loginUser getProperty:@"name"] password:loginUser.password response:^(BackendlessUser *userLogged) {
        [SVProgressHUD dismiss];
        [SqbLogger setLastAgentUserName:[loginUser getProperty:@"name"]] ;
        [SqbLogger setAgentPassword:[loginUser getProperty:@"password"]] ;

        
        
        
        
        BOOL isApproved = [[userLogged getProperty:@"approved"] boolValue];
        if(isApproved){
            [backendless.userService setStayLoggedIn:YES] ;
            [[self getViewController] performSegueWithIdentifier:@"loginToList" sender:self];
            

            
            [userLogged updateProperties:@{@"loginStatus" : @"loggedIn"}];
            [backendless.userService update:userLogged response:^(BackendlessUser *newUSer) {
                backendless.userService.currentUser = newUSer;
            } error:^(Fault *error) {
                NSLog(@"Error at the property update %@" , error.description);
            }];

            
            
        }
        else{
            
            
            if (backendless.userService.currentUser) {
                [backendless.userService.currentUser updateProperties:@{@"loginStatus" : @"loggedIn"}];
                [backendless.userService update:backendless.userService.currentUser response:^(BackendlessUser *newUSer) {

                } error:^(Fault *error) {
                    NSLog(@"Error at the property update %@" , error.description);
                }];
            }

            [backendless.userService logout:^(id user) {
                NSLog(@"logged out");
                backendless.userService.currentUser = nil;
            } error:^(Fault *error) {
                NSLog(@"error at log out");
            }
            ];
            AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"" andText:@"Your application to join RealKnu is not approved yet!" andCancelButton:NO forAlertType:AlertFailure];
            [alert show];
            
        }
    } error:^(Fault *error){
        
        [SVProgressHUD dismiss];
        
        
        if ([error.faultCode isEqualToString:@"3033"]){
            [SVProgressHUD dismiss];
            AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Login Failed!!!" andText:@"This UserName is already Registered as an Agent, Please Register with another User Name on previous screen." andCancelButton:NO forAlertType:AlertFailure];
            [alert show];
            return;
        }
        else if ([error.faultCode isEqualToString:@"3003"]){
            AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Login Failed!!!" andText:@"Invalid Username or Password" andCancelButton:NO forAlertType:AlertFailure];
            [alert show];
        }
        else if ([error.faultCode isEqualToString:@"-1009"]){
            AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Login Failed!!!" andText:@"The Internet connection appears to be offline." andCancelButton:NO forAlertType:AlertFailure];
            [alert show];
        }
        else{
            [SqbLogger logIt:[NSString stringWithFormat:@"%@ - %@" , error.faultCode , error.message ]] ;
            
            AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Oops!!!" andText:@"Something Unexpected has happened. Please Send Report to Developers." andCancelButton:YES forAlertType:AlertInfo andColor:[UIColor purpleColor] withCompletionHandler:^(AMSmoothAlertView *alertView, UIButton *btton) {
                if(btton == alertView.defaultButton) {
                    NSLog(@"Default");
                    [SqbLogger submitCrashReport:[NSString stringWithFormat:@"%@ - %@" , error.faultCode , error.message ]];
                    
                    [alert dismissAlertView];
                    
                }
                else {
                    NSLog(@"Others");
                    [alert dismissAlertView];
                    
                }
            }];
            [alert.defaultButton setTitle:@"Send Report" forState:UIControlStateNormal];
            [alert.defaultButton.titleLabel setFont: [alert.defaultButton.titleLabel.font fontWithSize: 12]];
            
            [alert.cancelButton setTitle:@"Close" forState:UIControlStateNormal];
            [alert.cancelButton.titleLabel setFont: [alert.defaultButton.titleLabel.font fontWithSize: 12]];
            
            
            [alert show];
            
        }
    }];
}


-(void)loginWithUserForFacebook :(BackendlessUser *)loginUser andFacebookResult :(id)result
{
    
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    [defaults setObject:nil forKey:@"QfistAndLastName"];
    [defaults setObject:nil forKey:@"companyName"];
    [defaults setObject:nil forKey:@"licenceNumber"];
    [defaults setObject:nil forKey:@"bestPhoneNo"];
    [defaults setObject:nil forKey:@"speciality"];
    [defaults setObject:nil forKey:@"Lenguage"];
    
    [defaults synchronize];

    
    
    
    [SVProgressHUD dismiss];
    [SVProgressHUD showWithStatus:@"Checking Credentials..."];
    [backendless.userService login:[loginUser getProperty:@"name"] password:loginUser.password response:^(BackendlessUser *userLogged) {
        [SVProgressHUD dismiss];
        [SqbLogger setLastAgentUserName:[loginUser getProperty:@"name"]] ;
        [SqbLogger setAgentPassword:[loginUser getProperty:@"password"]] ;

        BOOL isApproved = [[userLogged getProperty:@"approved"] boolValue];
        if(isApproved){
            [backendless.userService setStayLoggedIn:YES] ;
            [[self getViewController] performSegueWithIdentifier:@"loginToList" sender:self];
        }
        else{
            if (backendless.userService.currentUser) {
                [backendless.userService.currentUser updateProperties:@{@"loginStatus" : @"loggedIn"}];
                [backendless.userService update:backendless.userService.currentUser response:^(BackendlessUser *newUSer) {

                } error:^(Fault *error) {
                    NSLog(@"Error at the property update %@" , error.description);
                }];
            }
            [backendless.userService logout:^(id user) {
                NSLog(@"logged out");
                backendless.userService.currentUser = nil;
            } error:^(Fault *error) {
                NSLog(@"error at log out");
            }
             ];
            AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"" andText:@"Your application to join RealKnu is not approved yet!" andCancelButton:NO forAlertType:AlertFailure];
            [alert show];
            
        }
    } error:^(Fault *error){
        
        [SVProgressHUD dismiss];
        
        
        if([error.faultCode isEqualToString:@"3003"]){
            
            NSString *email = [NSString stringWithFormat:@"%@@Shirley.com" , [result objectForKey:@"id"]];
            if ([result objectForKey:@"email"]) {
                email = [result objectForKey:@"email"];
            }
            NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
            [defaults setObject:[result objectForKey:@"id"] forKey:@"username"];
            [defaults setObject:@"1223" forKey:@"password"];
            [defaults setObject:email forKey:@"email"];
            [defaults synchronize];
            [[self getViewController] performSegueWithIdentifier:@"SignUpToAnswerQuestions" sender:self] ;
            
        }
        else if ([error.faultCode isEqualToString:@"-1009"]){
            AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Login Failed!!!" andText:@"The Internet connection appears to be offline." andCancelButton:NO forAlertType:AlertFailure];
            [alert show];
        }
        else{
            [SqbLogger logIt:[NSString stringWithFormat:@"%@ - %@" , error.faultCode , error.message ]] ;
            
            AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Oops!!!" andText:@"Something Unexpected has happened. Please Send Report to Developers." andCancelButton:YES forAlertType:AlertInfo andColor:[UIColor purpleColor] withCompletionHandler:^(AMSmoothAlertView *alertView, UIButton *btton) {
                if(btton == alertView.defaultButton) {
                    NSLog(@"Default");
                    [SqbLogger submitCrashReport:[NSString stringWithFormat:@"%@ - %@" , error.faultCode , error.message ]];
                    
                    [alert dismissAlertView];
                    
                }
                else {
                    NSLog(@"Others");
                    [alert dismissAlertView];
                    
                }
            }];
            [alert.defaultButton setTitle:@"Send Report" forState:UIControlStateNormal];
            [alert.defaultButton.titleLabel setFont: [alert.defaultButton.titleLabel.font fontWithSize: 12]];
            
            [alert.cancelButton setTitle:@"Close" forState:UIControlStateNormal];
            [alert.cancelButton.titleLabel setFont: [alert.defaultButton.titleLabel.font fontWithSize: 12]];
            
            
            [alert show];
            
        }
    }];
}


- (IBAction)sSignUpFacebookAction:(UIButton *)sender {
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logInWithReadPermissions:@[@"public_profile",@"email"] fromViewController:[self getViewController] handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        if (result.isCancelled) {
            NSLog(@"Token: %@", result.token);
        } else {
            if ([FBSDKAccessToken currentAccessToken]) {
                FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc]initWithGraphPath:@"me" parameters:@{@"fields" : @"id,name,email,first_name,last_name,picture"                                                                                                            }];
                [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                    if (result) {
                        NSLog(@"Got in......");
                        NSString *email = [NSString stringWithFormat:@"%@@Shirley.com" , [result objectForKey:@"id"]];
                        if ([result objectForKey:@"email"]) {
                            email = [result objectForKey:@"email"];
                            isEmailGotten = YES;
                        }
                        else{
                            isEmailGotten = NO;
                        }
                        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
                        [defaults setObject:[result objectForKey:@"id"] forKey:@"username"];
                        [defaults setObject:@"1223" forKey:@"password"];
                        [defaults setObject:email forKey:@"email"];
                        [defaults setBool:isEmailGotten forKey:@"isEmailGotten"];
                        [defaults synchronize];
                        [[self getViewController] performSegueWithIdentifier:@"SignUpToAnswerQuestions" sender:self] ;
                        
                    }
                }];
            }
        }
    }];
}

- (IBAction)lLoginWithFacebookAction:(UIButton *)sender {
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logInWithReadPermissions:@[@"public_profile",@"email"] fromViewController:[self getViewController] handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        if (result.isCancelled) {
            NSLog(@"Token: %@", result.token);
        } else {
            if ([FBSDKAccessToken currentAccessToken]) {
                FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc]initWithGraphPath:@"me" parameters:@{@"fields" : @"id,name,email,first_name,last_name,picture"                                                                                                            }];
                [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                    if (result) {
                        NSLog(@"Got in......");
                        [SVProgressHUD showWithStatus:@"Loading..."];
                        NSString *email = [NSString stringWithFormat:@"%@@Shirley.com" , [result objectForKey:@"id"]];
                        if ([result objectForKey:@"email"]) {
                            email = [result objectForKey:@"email"];
                        }
                        BackendlessUser *user = [BackendlessUser new];
                        [user setProperty:@"name" object:[result objectForKey:@"id"]];
                        user.password = @"1223";
                        user.email = email;
                        [self loginWithUserForFacebook:user andFacebookResult:result];
                    }
                }];
            }
        }
    }];
}


@end
