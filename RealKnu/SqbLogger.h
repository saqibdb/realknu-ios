//
//  SqbLogger.h
//  RealKnu
//
//  Created by Shirley on 3/8/17.
//  Copyright © 2017 Jason Macbook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface SqbLogger : NSObject

+(void)setLastCustomerFirstName :(NSString *)firstName andLastName :(NSString *)lastName;
+(void)setLastCustomerEmail :(NSString *)email andPhone :(NSString *)phone ;
+(void)setLastAgentUserName :(NSString *)userName ;
+(void)setAgentPassword :(NSString *)password ;

+(NSString *)getCustomerFirstName ;

+(NSString *)getCustomerLastName ;

+(NSString *)getCustomerEmail ;

+(NSString *)getCustomerPhone ;

+(NSString *)getAgentUserName ;

+(NSString *)getAgentUPassword ;

+ (void)logIt:(NSString *)string ;

+ (void)submitCrashReport:(NSString *)message ;

@end
