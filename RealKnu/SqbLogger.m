//
//  SqbLogger.m
//  RealKnu
//
//  Created by Shirley on 3/8/17.
//  Copyright © 2017 Jason Macbook. All rights reserved.
//

#import "SqbLogger.h"
#import <SendGrid/SendGrid.h>
#import "SVProgressHUD.h"
#import "AMSmoothAlertView.h"


@implementation SqbLogger

+(void)setLastCustomerFirstName :(NSString *)firstName andLastName :(NSString *)lastName{
    [[NSUserDefaults standardUserDefaults] setObject:firstName forKey:@"lastFirstName"];
    [[NSUserDefaults standardUserDefaults] setObject:lastName forKey:@"lastLastName"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

+(void)setLastCustomerEmail :(NSString *)email andPhone :(NSString *)phone{
    [[NSUserDefaults standardUserDefaults] setObject:email forKey:@"lastEmail"];
    [[NSUserDefaults standardUserDefaults] setObject:phone forKey:@"lastPhone"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(void)setLastAgentUserName :(NSString *)userName {
    [[NSUserDefaults standardUserDefaults] setObject:userName forKey:@"lastUserName"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}


+(void)setAgentPassword :(NSString *)password {
    [[NSUserDefaults standardUserDefaults] setObject:password forKey:@"agentPassword"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

+(NSString *)getCustomerFirstName{
    NSString * myDictionary = [[NSUserDefaults standardUserDefaults] objectForKey:@"lastFirstName"];
    if (myDictionary) {
        return myDictionary;
    }
    else{
        return nil;
    }
}

+(NSString *)getCustomerLastName{
    NSString * myDictionary = [[NSUserDefaults standardUserDefaults] objectForKey:@"lastLastName"];
    if (myDictionary) {
        return myDictionary;
    }
    else{
        return nil;
    }
}

+(NSString *)getCustomerEmail{
    NSString * myDictionary = [[NSUserDefaults standardUserDefaults] objectForKey:@"lastEmail"];
    if (myDictionary) {
        return myDictionary;
    }
    else{
        return nil;
    }
}

+(NSString *)getCustomerPhone{
    NSString * myDictionary = [[NSUserDefaults standardUserDefaults] objectForKey:@"lastPhone"];
    if (myDictionary) {
        return myDictionary;
    }
    else{
        return nil;
    }
}


+(NSString *)getAgentUserName{
    NSString * myDictionary = [[NSUserDefaults standardUserDefaults] objectForKey:@"lastUserName"];
    if (myDictionary) {
        return myDictionary;
    }
    else{
        return nil;
    }
}

+(NSString *)getAgentUPassword{
    NSString * myDictionary = [[NSUserDefaults standardUserDefaults] objectForKey:@"agentPassword"];
    if (myDictionary) {
        return myDictionary;
    }
    else{
        return nil;
    }
}

+ (void)logIt:(NSString *)string {
    // First send the string to NSLog
    NSLog(@"%@", string);
    
    // Setup date stuff
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"YYYY-dd-MM"];
    NSDate *date = [NSDate date];
    
    // Paths - We're saving the data based on the day.
    //NSString *path = [NSString stringWithFormat:@"%@-logFile.txt", [formatter stringFromDate:date]];
    NSString *path = [NSString stringWithFormat:@"RealKnu-logFile.txt"];
    NSString *writePath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:path];
    
    // We're going to want to append new data, so get the previous data.
    NSString *fileContents = [NSString stringWithContentsOfFile:writePath encoding:NSUTF8StringEncoding error:nil];
    
    // Write it to the string
    string = [NSString stringWithFormat:@"%@\n%@ - %@_____end_____", fileContents, [formatter stringFromDate:date], string];
    
    // Write to file stored at: "~/Library/Application\ Support/iPhone\ Simulator/*version*/Applications/*appGUID*/Documents/*date*-logFile.txt"
    [string writeToFile:writePath atomically:YES encoding:NSUTF8StringEncoding error:nil];
}

+ (void)submitCrashReport:(NSString *)message {
    
}



@end
