//
//  TermsAndConditionsPopUp.h
//  RealKnu
//
//  Created by Shirley-Mac3 on 2/8/17.
//  Copyright © 2017 Jason Macbook. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TermsAndConditionsPopUp : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *closeBtn;
@property (weak, nonatomic) IBOutlet UIButton *acceptBtn;
@property (weak, nonatomic) IBOutlet UITextView *textView;

@end
