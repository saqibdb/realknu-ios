//
//  TermsAndConditionsPopUp.m
//  RealKnu
//
//  Created by Shirley-Mac3 on 2/8/17.
//  Copyright © 2017 Jason Macbook. All rights reserved.
//

#import "TermsAndConditionsPopUp.h"

@interface TermsAndConditionsPopUp ()

@end

@implementation TermsAndConditionsPopUp

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    // Do any additional setup after loading the view.
}
- (void)viewDidLayoutSubviews {
    [self.textView setContentOffset:CGPointZero animated:NO];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
