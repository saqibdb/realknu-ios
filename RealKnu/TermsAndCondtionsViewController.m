//
//  TermsAndCondtionsViewController.m
//  RealKnu
//
//  Created by Shirley-Mac3 on 2/2/17.
//  Copyright © 2017 Jason Macbook. All rights reserved.
//

#import "TermsAndCondtionsViewController.h"

@interface TermsAndCondtionsViewController ()

@end

@implementation TermsAndCondtionsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.popUpView setHidden:YES];
    [self.triangleSprite setHidden:YES];
    [self maskTriananlge:self.triangleSprite];
    
    // Do any additional setup after loading the view.
}
-(void)viewDidAppear:(BOOL)animated{
    [self.popUpView setHidden:YES];
    [self.triangleSprite setHidden:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)maskTriananlge:(UIView *)view {
    
    [self.view layoutIfNeeded];
    UIBezierPath *path = [UIBezierPath new];
    [path moveToPoint:(CGPoint){view.frame.size.width / 2, 0}];
    [path addLineToPoint:(CGPoint){0, view.frame.size.height}];
    [path addLineToPoint:(CGPoint){view.frame.size.width, view.frame.size.height}];
    [path addLineToPoint:(CGPoint){view.frame.size.width / 2, 0}];
    
    CAShapeLayer *mask = [CAShapeLayer new];
    mask.frame = view.bounds;
    mask.path = path.CGPath;
    view.layer.mask = mask;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)backAction:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)menuAction:(UIButton *)sender {
    if ([self.popUpView isHidden]) {
        [self.popUpView setHidden:NO];
        [self.triangleSprite setHidden:NO];
    }
    else
    {
        [self.popUpView setHidden:YES];
        [self.triangleSprite setHidden:YES];
    }
}



@end
