//
//  UserCellTableViewCell.h
//  RealKnu
//
//  Created by Shirley-Mac3 on 2/3/17.
//  Copyright © 2017 Jason Macbook. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWTableViewCell.h"
@interface UserCellTableViewCell : SWTableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *profileImag;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *phone;

@end
