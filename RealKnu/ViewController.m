//
//  ViewController.m
//  RealKnu
//
//  Created by Shirley-Mac3 on 2/1/17.
//  Copyright © 2017 Jason Macbook. All rights reserved.
//

#import "ViewController.h"
#import "SVProgressHUD.h"
#import "Backendless.h"
#import "AMSmoothAlertView.h"
#import "SVProgressHUD.h"
#import <INTULocationManager/INTULocationManager.h>
#import "SqbLogger.h"

@interface ViewController ()
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
}

-(void)viewDidAppear:(BOOL)animated{
    [SVProgressHUD dismiss];

     [self checkAutoLogins];
}


-(void)checkAutoLogins {
    [SVProgressHUD show];

    if (backendless.userService.currentUser) {
        
        
            [backendless.userService isValidUserToken:
             ^(NSNumber *result) {
                 [SVProgressHUD dismiss];
                 NSLog(@"isValidUserToken (ASYNC): %@", [result boolValue]?@"YES":@"NO");
                 
                 if ([[backendless.userService.currentUser getProperty:@"type"] isEqualToString:@"agent"]){
                     
                     BOOL isApproved = [[backendless.userService.currentUser getProperty:@"approved"] boolValue];
                     if(isApproved){
                         
                         [backendless.userService.currentUser updateProperties:@{@"loginStatus" : @"loggedIn"}];
                         [backendless.userService update:backendless.userService.currentUser response:^(BackendlessUser *newUSer) {
                             backendless.userService.currentUser = newUSer;
                         } error:^(Fault *error) {
                             NSLog(@"Error at the property update %@" , error.description);
                         }];
                         
                         
                         
                         [self performSegueWithIdentifier:@"ToAutoLogin" sender:self];

                     }
                     else{
                         if (backendless.userService.currentUser) {
                             [backendless.userService.currentUser updateProperties:@{@"loginStatus" : @"loggedIn"}];
                             [backendless.userService update:backendless.userService.currentUser response:^(BackendlessUser *newUSer) {

                             } error:^(Fault *error) {
                                 NSLog(@"Error at the property update %@" , error.description);
                             }];
                         }
                         [backendless.userService logout:^(id user) {
                             NSLog(@"logged out");
                             backendless.userService.currentUser = nil;
                         } error:^(Fault *error) {
                             NSLog(@"error at log out");
                         }
                          ];
                         AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"" andText:@"Your application to join RealKnu is not approved yet!" andCancelButton:NO forAlertType:AlertFailure];
                         [alert show];
                         
                     }
                     
                 }
                 else{
                     [self performSegueWithIdentifier:@"viewControllerToMap" sender:self];
                 }
             }
                                                error:^(Fault *fault) {
                                                    
                                                    
                                                    NSLog(@"login FAULT (ASYNC): %@", fault);
                                                    if ([fault.faultCode isEqualToString:@"3064"] ) {
                                                        BackendlessUser *user = backendless.userService.currentUser ;
                                                        user.password = @"123" ;
                                                        
                                                        [self loginWithUser:user] ;
                                                    }
                                                    else {
                                                        [SVProgressHUD dismiss];
                                                        
                                                        [self performSegueWithIdentifier:@"viewControllerToRphaseOne" sender:self];
                                                    }
                                                }];
        }
    
    
    else{
        [SVProgressHUD dismiss];
    }

}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)unwindToViewController:(UIStoryboardSegue*)sender
{
    
}
- (IBAction)customerAction:(UIButton *)sender {
    [self validUserTokenAsync];
    //[self performSegueWithIdentifier:@"viewControllerToRphaseOne" sender:self];
    
}
-(void)validUserTokenAsync {
    
    [SVProgressHUD show];
    if (backendless.userService.currentUser) {
        [backendless.userService isValidUserToken:
         ^(NSNumber *result) {
             [SVProgressHUD dismiss];
             NSLog(@"isValidUserToken (ASYNC): %@", [result boolValue]?@"YES":@"NO");
             
             if ([[backendless.userService.currentUser getProperty:@"type"] isEqualToString:@"agent"])
             {
                 
                 [backendless.userService.currentUser updateProperties:@{@"loginStatus" : @"loggedIn"}];
                 [backendless.userService update:backendless.userService.currentUser response:^(BackendlessUser *newUSer) {
                     backendless.userService.currentUser = newUSer;
                 } error:^(Fault *error) {
                     NSLog(@"Error at the property update %@" , error.description);
                 }];
                 
                 [self performSegueWithIdentifier:@"ToAutoLogin" sender:self];
                 
                 
                 
                 
                 
                 
             }
             else{
                 [self performSegueWithIdentifier:@"viewControllerToMap" sender:self];
             }
         }
        error:^(Fault *fault) {
                                                
                                                
                                                NSLog(@"login FAULT (ASYNC): %@", fault);
                                                if ([fault.faultCode isEqualToString:@"3064"] ) {
                                                    BackendlessUser *user = backendless.userService.currentUser ;
                                                    user.password = @"123" ;
                                                    
                                                    [self loginWithUser:user] ;
                                                }
                                                else {
                                                    [SVProgressHUD dismiss];
                                                    
                                                    [self performSegueWithIdentifier:@"viewControllerToRphaseOne" sender:self];
                                                }
                                            }];
        
        
    }else{
        [SVProgressHUD dismiss];
        [self performSegueWithIdentifier:@"viewControllerToRphaseOne" sender:self];
        
        
    }
    
    NSLog(@"______%@_______" , backendless.userService.currentUser.name) ;
    
}
-(void)loginWithUser :(BackendlessUser *)loginUser
{
    [SVProgressHUD show];
    [backendless.userService login:loginUser.email password:loginUser.password response:^(BackendlessUser *userLogged) {
        [SVProgressHUD dismiss];
        
        [self performSegueWithIdentifier:@"viewControllerToMap" sender:self];
        
    }
                             error:^(Fault *error) {
                                 
                                 [SVProgressHUD dismiss];
                                 
                                 if ([error.faultCode isEqualToString:@"3033"]){
                                     [SVProgressHUD dismiss];
                                     AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Login Failed!!!" andText:@"This UserName is already Registered as an Agent, Please Register with another User Name on previous screen." andCancelButton:NO forAlertType:AlertFailure];
                                     [alert show];
                                     return;
                                 }
                                 
                                 if ([error.faultCode isEqualToString:@"-1009"]){
                                     AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Login Failed!!!" andText:@"The Internet connection appears to be offline." andCancelButton:NO forAlertType:AlertFailure];
                                     [alert show];
                                     return;
                                 }
                                 
                                 [SqbLogger logIt:[NSString stringWithFormat:@"%@ - %@" , error.faultCode , error.message ]] ;
                                 
                                 AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Oops!!!" andText:@"Something Unexpected has happened. Please Send Report to Developers." andCancelButton:YES forAlertType:AlertInfo andColor:[UIColor purpleColor] withCompletionHandler:^(AMSmoothAlertView *alertView, UIButton *btton) {
                                     if(btton == alertView.defaultButton) {
                                         NSLog(@"Default");
                                         [SqbLogger submitCrashReport:[NSString stringWithFormat:@"%@ - %@" , error.faultCode , error.message ]];
                                         
                                         [alert dismissAlertView];
                                         
                                     }
                                     else {
                                         NSLog(@"Others");
                                         [alert dismissAlertView];
                                         
                                     }
                                 }];
                                 [alert.defaultButton setTitle:@"Send Report" forState:UIControlStateNormal];
                                 [alert.defaultButton.titleLabel setFont: [alert.defaultButton.titleLabel.font fontWithSize: 12]];
                                 
                                 [alert.cancelButton setTitle:@"Close" forState:UIControlStateNormal];
                                 [alert.cancelButton.titleLabel setFont: [alert.defaultButton.titleLabel.font fontWithSize: 12]];
                                 
                                 
                                 [alert show];

                                 
                             }];
}

@end
