//
//  main.m
//  RealKnu
//
//  Created by Shirley-Mac3 on 2/1/17.
//  Copyright © 2017 Jason Macbook. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
